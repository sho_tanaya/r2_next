name := "r2-next"

version := "0.1"

scalaVersion := "2.11.12"
libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.2.0" % "provided",
  "org.apache.spark" %% "spark-sql" % "2.2.0" % "provided",
  "org.apache.spark" %% "spark-hive" % "2.2.0" % "provided",
  "org.apache.spark" %% "spark-mllib" % "2.2.0" % "provided",
  "joda-time" % "joda-time" % "2.0",
  "org.joda" % "joda-convert" % "1.1",
  "org.apache.lucene" % "lucene-core" % "7.3.0",
  "org.apache.lucene" % "lucene-analyzers-common" % "7.3.0",
  "org.apache.lucene" % "lucene-analyzers-kuromoji" % "7.3.0",
  "org.apache.lucene" % "lucene-queryparser" % "7.3.0",
  "com.typesafe.play" %% "play" % "2.6.3",
  "org.apache.mahout" % "mahout-math" % "0.9"
)
