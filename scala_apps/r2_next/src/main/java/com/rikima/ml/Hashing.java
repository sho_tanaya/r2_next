package com.rikima.ml;

import com.google.common.base.Charsets;
import org.apache.mahout.math.MurmurHash;

/**
 * Created by mrikitoku on 15/10/13.
 */
public class Hashing {
    public static final int seed = 20151013;

    private static final byte[] EMPTY_ARRAY = new byte[0];

    protected static byte[] bytesForString(String x) {
        return x == null ? EMPTY_ARRAY : x.getBytes(Charsets.UTF_8);
    }

    public static int hash(String term, int probe, int numFeatures) {
        long r = MurmurHash.hash64A(bytesForString(term), probe) % numFeatures;
        if (r < 0) {
            r += numFeatures;
        }
        return (int) r;
    }
}

