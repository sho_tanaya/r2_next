package com.d2c.be.r2_next.nlp;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CharacterNormalizer {
    public enum CharacterType {
        LOWER_CASE_ALPHABET, ZENKAKU_LOWER_CASE_ALPHABET, UPPER_CASE_ALPHABET, ZENNKAKU_UPPER_CASE_ALPHABET, ALPHABET,
        HANKAKU_HIRAGANA, ZENKAKU_HIRAGANA, HIRAGANA,
        HANKAKU_KATAKANA, ZENKAKU_KATAKANA, KATAKANA,
        HANKAKU_SYMBOL, ZENKAKU_SYMBOL, SYMBOL,
        KANJI,
        NUMBER,
        OTHER
    }

    //public static String ZENKAKU_KATAKANA = "アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワン";
    private static final char[] HANKAKU_KATAKANA_SET = { '｡', '｢', '｣', '､', '･',
        'ｦ', 'ｧ', 'ｨ', 'ｩ', 'ｪ', 'ｫ', 'ｬ', 'ｭ', 'ｮ', 'ｯ', 'ｰ', 'ｱ', 'ｲ',
        'ｳ', 'ｴ', 'ｵ', 'ｶ', 'ｷ', 'ｸ', 'ｹ', 'ｺ', 'ｻ', 'ｼ', 'ｽ', 'ｾ', 'ｿ',
        'ﾀ', 'ﾁ', 'ﾂ', 'ﾃ', 'ﾄ', 'ﾅ', 'ﾆ', 'ﾇ', 'ﾈ', 'ﾉ', 'ﾊ', 'ﾋ', 'ﾌ',
        'ﾍ', 'ﾎ', 'ﾏ', 'ﾐ', 'ﾑ', 'ﾒ', 'ﾓ', 'ﾔ', 'ﾕ', 'ﾖ', 'ﾗ', 'ﾘ', 'ﾙ',
        'ﾚ', 'ﾛ', 'ﾜ', 'ﾝ', 'ﾞ', 'ﾟ' };

    private static final char[] ZENKAKU_KATAKANA_SET = { '。', '「', '」', '、', '・',
        'ヲ', 'ァ', 'ィ', 'ゥ', 'ェ', 'ォ', 'ャ', 'ュ', 'ョ', 'ッ', 'ー', 'ア', 'イ',
        'ウ', 'エ', 'オ', 'カ', 'キ', 'ク', 'ケ', 'コ', 'サ', 'シ', 'ス', 'セ', 'ソ',
        'タ', 'チ', 'ツ', 'テ', 'ト', 'ナ', 'ニ', 'ヌ', 'ネ', 'ノ', 'ハ', 'ヒ', 'フ',
        'ヘ', 'ホ', 'マ', 'ミ', 'ム', 'メ', 'モ', 'ヤ', 'ユ', 'ヨ', 'ラ', 'リ', 'ル',
        'レ', 'ロ', 'ワ', 'ン', '゛', '゜' };

    private static final char[] SYMBOL = {
        '/', '!', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/',
        ':', ';', '<', '=', '>', '?', '[', '\\', ']', '^', '_', '{', '|', '}', '~'
     };
    
    private static final char[] ZENKAKU_SYMBOL = {
        '／', '！', '＃', '＄', '％', '＆', '’', '（', '）', '＊', '＋', '，', '－', '．', '／',
        '：', '；', '＜', '＝', '＞', '？', '［', '\\', '］', '＾', '＿', '｛', '｜', '｝', '～'
     };
    

    private static final char HANKAKU_KATAKANA_FIRST_CHAR = HANKAKU_KATAKANA_SET[0];

    private static final char HANKAKU_KATAKANA_LAST_CHAR = HANKAKU_KATAKANA_SET[HANKAKU_KATAKANA_SET.length - 1];

    private static Map<Integer, Integer>hankakuSymbol2Zenkaku = new ConcurrentHashMap<Integer, Integer>();
    static {
        for (int i = 0;i < SYMBOL.length; ++i) {
            int c = SYMBOL[i];
            int zc = ZENKAKU_SYMBOL[i];
            hankakuSymbol2Zenkaku.put(c, zc);
        }
    }

    public static char hankakuKatakanaToZenkakuKatakana(char c) {
        if (c >= HANKAKU_KATAKANA_FIRST_CHAR && c <= HANKAKU_KATAKANA_LAST_CHAR) {
            return ZENKAKU_KATAKANA_SET[c - HANKAKU_KATAKANA_FIRST_CHAR];
        } else {
            return c;
        }
    }

    public static char hankakuSymbolToZenkaku(char c) {
        if (hankakuSymbol2Zenkaku.containsKey((int)c)) {
            return (char)hankakuSymbol2Zenkaku.get((int)c).intValue();
        } else {
            return c;
        }
    }

    /**
     * 2文字目が濁点・半濁点で、1文字目に加えることができる場合は、合成した文字を返します。
     * 合成ができないときは、c1を返します。
     * @param c1 変換前の1文字目
     * @param c2 変換前の2文字目
     * @return 変換後の文字
     */
    public static char mergeChar(char c1, char c2) {
        if (c2 == 'ﾞ' || c2 == '゛') {
            if ("ｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾊﾋﾌﾍﾎｳ".indexOf(c1) >= 0) {
                switch (c1) {
                    case 'ｶ': return 'ガ';
                    case 'ｷ': return 'ギ';
                    case 'ｸ': return 'グ';
                    case 'ｹ': return 'ゲ';
                    case 'ｺ': return 'ゴ';
                    case 'ｻ': return 'ザ';
                    case 'ｼ': return 'ジ';
                    case 'ｽ': return 'ズ';
                    case 'ｾ': return 'ゼ';
                    case 'ｿ': return 'ゾ';
                    case 'ﾀ': return 'ダ';
                    case 'ﾁ': return 'ヂ';
                    case 'ﾂ': return 'ヅ';
                    case 'ﾃ': return 'デ';
                    case 'ﾄ': return 'ド';
                    case 'ﾊ': return 'バ';
                    case 'ﾋ': return 'ビ';
                    case 'ﾌ': return 'ブ';
                    case 'ﾍ': return 'ベ';
                    case 'ﾎ': return 'ボ';
                    case 'ｳ': return 'ヴ';
                }
            } else if ("カキクケコサシスセソタチツテトハヒフヘホウ".indexOf(c1) >= 0) {
                switch (c1) {
                    case 'カ': return 'ガ';
                    case 'キ': return 'ギ';
                    case 'ク': return 'グ';
                    case 'ケ': return 'ゲ';
                    case 'コ': return 'ゴ';
                    case 'サ': return 'ザ';
                    case 'シ': return 'ジ';
                    case 'ス': return 'ズ';
                    case 'セ': return 'ゼ';
                    case 'ソ': return 'ゾ';
                    case 'タ': return 'ダ';
                    case 'チ': return 'ヂ';
                    case 'ツ': return 'ヅ';
                    case 'テ': return 'デ';
                    case 'ト': return 'ド';
                    case 'ハ': return 'バ';
                    case 'ヒ': return 'ビ';
                    case 'フ': return 'ブ';
                    case 'ヘ': return 'ベ';
                    case 'ホ': return 'ボ';
                    case 'ウ': return 'ヴ';
                }
            } else if ("かきくけこさしすせそたちつてとはひふへほう".indexOf(c1) >= 0) {
                switch (c1) {
                    case 'か': return 'が';
                    case 'き': return 'ぎ';
                    case 'く': return 'ぐ';
                    case 'け': return 'げ';
                    case 'こ': return 'ご';
                    case 'さ': return 'ざ';
                    case 'し': return 'じ';
                    case 'す': return 'ず';
                    case 'せ': return 'ぜ';
                    case 'そ': return 'ぞ';
                    case 'た': return 'だ';
                    case 'ち': return 'ぢ';
                    case 'つ': return 'づ';
                    case 'て': return 'で';
                    case 'と': return 'ど';
                    case 'は': return 'ば';
                    case 'ひ': return 'び';
                    case 'ふ': return 'ぶ';
                    case 'へ': return 'べ';
                    case 'ほ': return 'ぼ';
                    case 'う': return 'ヴ';
                } 
            }
        } else if (c2 == 'ﾟ' || c2 == '゜') {
            if ("ﾊﾋﾌﾍﾎ".indexOf(c1) >= 0) {
                switch (c1) {
                    case 'ﾊ': return 'パ';
                    case 'ﾋ': return 'ピ';
                    case 'ﾌ': return 'プ';
                    case 'ﾍ': return 'ペ';
                    case 'ﾎ': return 'ポ';
                }
            } else if ("ハヒフヘホ".indexOf(c1) >= 0) {
                switch (c1) {
                    case 'ハ': return 'パ';
                    case 'ヒ': return 'ピ';
                    case 'フ': return 'プ';
                    case 'ヘ': return 'ペ';
                    case 'ホ': return 'ポ';
                }
            } else if ("はひふへほ".indexOf(c1) >= 0) {
                switch (c1) {
                    case 'は': return 'ぱ';
                    case 'ひ': return 'ぴ';
                    case 'ふ': return 'ぷ';
                    case 'へ': return 'ぺ';
                    case 'ほ': return 'ぽ';
                }
            }
        }
        return c1;
    }

    /**
     * 文字列中の半角カタカナを全角カタカナに変換します。
     * @param s 変換前文字列
     * @return 変換後文字列
     */
    public static String hankakuKatakanaToZenkakuKatakana(String s) {
        if (s.length() == 0) {
            return s;
        } else if (s.length() == 1) {
            return hankakuKatakanaToZenkakuKatakana(s.charAt(0)) + "";
        } else {
            StringBuffer sb = new StringBuffer(s);
            int i = 0;
            for (i = 0; i < sb.length() - 1; i++) {
                char originalChar1 = sb.charAt(i);
                char originalChar2 = sb.charAt(i + 1);
                char margedChar = mergeChar(originalChar1, originalChar2);
                if (margedChar != originalChar1) {
                    sb.setCharAt(i, margedChar);
                    sb.deleteCharAt(i + 1);
                } else {
                    char convertedChar = hankakuKatakanaToZenkakuKatakana(originalChar1);
                    if (convertedChar != originalChar1) {
                        sb.setCharAt(i, convertedChar);
                    }
                }
            }
            if (i < sb.length()) {
                char originalChar1 = sb.charAt(i);
                char convertedChar = hankakuKatakanaToZenkakuKatakana(originalChar1);
                if (convertedChar != originalChar1) {
                    sb.setCharAt(i, convertedChar);
                }
            }
            return sb.toString();
        }
    }

    public static boolean isAllHiragana(String text) {
        for (int i = 0;i < text.length(); ++i) {
            char c = text.charAt(i);
            if (c != ' ' && (c <  'ぁ' || c > 'ん')) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAllAlphabet(String text) {
        for (int i = 0;i < text.length(); ++i) {
            char c = text.charAt(i);
            if (c != ' ' && (c <  'a' || c > 'z')) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAllKatakana(String text) {
        char cs = 'ァ';
        char ce = 'ん' - 'ぁ' + 'ァ';
        for (int i = 0;i < text.length(); ++i) {
            char c = text.charAt(i);
            if (c != ' ' && (c < cs || c > ce)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAllNumber(String text) {
        for (int i = 0;i < text.length(); ++i) {
            char c = text.charAt(i);
            if (c != ' ' && (c <  '0' || c > '9')) {
                return false;
            }
        }
        return true;
    }

    public static String transformHankakuToZenkaku(String text) {
        StringBuffer temp = new StringBuffer();
        for (int i = 0; i < text.length(); i++) {
            char character = text.charAt(i);
            // System.out.println(i + " - " + ch);
            // 全角数字を半角数字に変換
            if (character >= '０' && character <= '９') {
                temp.append((char) (character - '０' + '0'));
                continue;
            }

            // 全角英字（大文字）を半角英字（小文字）に変換
            if (character >= 'Ａ' && character <= 'Ｚ') {
                temp.append((char) (character - 'Ａ' + 'a'));
                continue;
            }

            // 全角英字（小文字）を半角英字（小文字）に変換
            if (character >= 'ａ' && character <= 'ｚ') {
                temp.append((char) (character - 'ａ' + 'a'));
                continue;
            }

            // 半角英字（大文字）を半角英字（小文字）に変換
            if (character >= 'A' && character <= 'Z') {
                temp.append((char) (character - 'A' + 'a'));
                continue;
            }

            if ('゛' == character && 0 < i && 'う' == text.charAt(i - 1)) {
                temp.replace(temp.length() - 1, temp.length(), "ヴ");
                continue;
            }
            // 全角記号に
            character = hankakuSymbolToZenkaku(character);

            // 変換が不要な文字
            temp.append(character);
        }
        return temp.toString();
    }
   
    public static String normalize(String text, boolean replaceSpace) {
        // 半角カタカナを全角に
        String at = CharacterNormalizer.hankakuKatakanaToZenkakuKatakana(text);
        // 全角アルファベットは半角に
        at = CharacterNormalizer.transformHankakuToZenkaku(at);
        // 半角スペースは全角に
        if (replaceSpace) {
            at = at.replace(" ", "　");
        }
        // アルファベットは小文字に
        try {
            at = at.toLowerCase();
        } catch (java.lang.InternalError e) {
            e.printStackTrace();
        }

        at = htmlUnescape(at);
        return at;
    }

    public static String htmlUnescape(String text) {

        return text
                .replace("&quot;", "\"")
                .replace("&amp;", "&")
                .replace("&lt;", "<")
                .replace("&gt;", ">")
                .replace("&nbsp;", " ");
    }

    public static boolean isAllSymbol(String text) {
        for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);
            boolean symbol = false;
            for (int j = 0; j < SYMBOL.length; ++j) {
                if (c == SYMBOL[j]) {
                    symbol = true;
                    break;
                }
            }
            if (!symbol) {
                return false;
            }
        }
        return true;
    }

    public static CharacterType getCharacterType(String text) {
        if (isAllAlphabet(text)) {
            return CharacterType.ALPHABET;
        } else if (isAllHiragana(text)) {
            return CharacterType.HIRAGANA;
        } else if (isAllKatakana(text)) {
            return CharacterType.KATAKANA;
        } else if (isAllNumber(text)) {
            return CharacterType.NUMBER;
        } else if (isAllSymbol(text)) {
            return CharacterType.SYMBOL;
        } else {
            return CharacterType.OTHER;
        }
    }
}
