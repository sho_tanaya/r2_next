package com.d2c.be.r2_next.nlp;

//import org.omg.PortableInterceptor.NON_EXISTENT;

/**
 * Created by mrikitoku on 14/12/09.
 */
public class CharacterSplitter {
    private static byte[] characterTerminations = new byte[128];

    public void split(String text) {
        if (text.length() > characterTerminations.length) {
            characterTerminations = new byte[text.length()];
        }
        for (int i = 0; i < characterTerminations.length;++i) {
            characterTerminations[i] = 0;
        }
        CharacterTable.CharacterType prev = null;
        int same = 0;
        for (int i = 0;i < text.length(); ++i) {
            CharacterTable.CharacterType ct = CharacterTable.getCharacterType(text.charAt(i));
            if (ct != prev && same > 1) {
                characterTerminations[i-1] = 1;
                same = 0;
            } else {
                ++same;
            }
            prev = ct;
        }
    }

    public static String splitedText(String text) {
        text = CharacterNormalizer.normalize(text, false);
        StringBuilder sb = new StringBuilder();
        CharacterTable.CharacterType prev = null;
        char prevChar = Character.NON_SPACING_MARK;
        for (int i = 0;i < text.length(); ++i) {
            char c = text.charAt(i);
            if (sb.length() > 0) {
                prevChar = sb.charAt(sb.length()-1);
            }
            CharacterTable.CharacterType ct = CharacterTable.getCharacterType(text.charAt(i));
            if (ct != prev && prev != null && prevChar != ' ') {
                sb.append(" ");
            }
            if (c != ' ') {
                sb.append(c);
            }
            prev     = ct;
        }
        return sb.toString();
    }
}