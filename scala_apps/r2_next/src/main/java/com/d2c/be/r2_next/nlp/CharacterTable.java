package com.d2c.be.r2_next.nlp;

import java.lang.Character.UnicodeBlock;

public class CharacterTable {
    public enum CharacterType {
        // alphabet
        LOWER_CASE_ALPHABET, ZENKAKU_LOWER_CASE_ALPHABET,
        UPPER_CASE_ALPHABET, ZENKAKU_UPPER_CASE_ALPHABET,
        // ひらがな
        HIRAGANA,
        // カタカナ
        KATAKANA, HANKAKU_KATAKANA,
        NUMERIC, ZENKAKU_NUMERIC,
        KANJI,
        OTHER
    }

    // alphabet
    public static final char FIRST_LOWER_CASE_ALPHABET = 'a';
    public static final char FIRST_ZENKAKU_LOWER_CASE_ALPHABET = 'ａ';
    
    public static final char FIRST_UPPER_CASE_ALPHABET = 'A';
    public static final char FIRST_ZENKAKU_UPPER_CASE_ALPHABET = 'Ａ';

    public static int ALPHABET_CARDINALITY = 26;
    
    public static final String HIRAGANA = "ぁあぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢっつづてでとどなにぬねのはばぱひびぴふぶぷへべぺほぼぽまみむめもゃやゅゆょよらりるれろゎわゐゑをん";
    public static final char FIRST_HIRAGANA = 'あ';
    public static final int HIRAGANA_CARDINALITY = HIRAGANA.length();

    public static final String KATAKANA = "ァアィイゥウェエォオカガキギクグケゲコゴサザシジスズセゼソゾタダチヂッツヅテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモャヤュユョヨラリルレロヮワヰヱヲンヴヵヶ";
    public static final char FIRST_KATAKANA = 'ア';
    public static final char FIRST_HANKAKU_KATAKANA = 'ｱ';
    public static final int KATAKANA_CARDINALITY = KATAKANA.length();

    public static final String NUMERIC = "0123456789";
    public static final String ZENKAKU_NUMERIC = "０１２３４５６７８９";

    /**
     * 文字種を取得する。
     * @param c
     * @return
     */
    public static CharacterType getCharacterType(char c) {
        if (c >= FIRST_LOWER_CASE_ALPHABET && c <= FIRST_LOWER_CASE_ALPHABET + ALPHABET_CARDINALITY) {
            return CharacterType.LOWER_CASE_ALPHABET;
        } else if (c >= FIRST_ZENKAKU_LOWER_CASE_ALPHABET && c <= FIRST_ZENKAKU_LOWER_CASE_ALPHABET + ALPHABET_CARDINALITY) {
            return CharacterType.ZENKAKU_LOWER_CASE_ALPHABET;
        } else if (c >= FIRST_UPPER_CASE_ALPHABET && c <= FIRST_UPPER_CASE_ALPHABET + ALPHABET_CARDINALITY) {
            return CharacterType.UPPER_CASE_ALPHABET;
        } else if (c >= FIRST_ZENKAKU_UPPER_CASE_ALPHABET && c <= FIRST_ZENKAKU_UPPER_CASE_ALPHABET + ALPHABET_CARDINALITY) {
            return CharacterType.ZENKAKU_UPPER_CASE_ALPHABET;
        } else if (c >= FIRST_HIRAGANA && c <= FIRST_HIRAGANA + HIRAGANA_CARDINALITY) {
            return CharacterType.HIRAGANA;
        } else if (c >= FIRST_KATAKANA && c <= FIRST_KATAKANA + KATAKANA_CARDINALITY || c == 'ー') {
            return CharacterType.KATAKANA;
        } else if (c >= FIRST_HANKAKU_KATAKANA && c <= FIRST_HANKAKU_KATAKANA + ALPHABET_CARDINALITY) {
            return CharacterType.HANKAKU_KATAKANA;
        } else if (c >= '1' && c <= '1' + 10) {
            return CharacterType.NUMERIC;
        } else if (c >= '１' && c <= '１' + 10) {
            return CharacterType.ZENKAKU_NUMERIC;
        } else if (UnicodeBlock.of(c) == UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS) {
            return CharacterType.KANJI;
        } else {
            return CharacterType.OTHER;
        }
    }
}
