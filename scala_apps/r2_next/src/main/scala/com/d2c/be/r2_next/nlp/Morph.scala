package com.d2c.be.r2_next.nlp

/**
 * Created by mrikitoku on 15/09/16.
 */
class Morph(rep: String) extends Serializable {
  val surface = rep.split(Morph.DELIMITER)(0)
  val clazz   = rep.split(Morph.DELIMITER)(1)
  val dClazz  = rep.split(Morph.DELIMITER)(2)

  override def toString : String = {
    s"$surface,$clazz,$dClazz"
  }
}

object Morph {
  val DELIMITER = ","
  def apply(rep: String) : Morph = {
    if (rep != null && rep.trim.length > 0 && rep.split(DELIMITER).size == 3) {
      new Morph(rep)
    } else {
      null
    }
  }
}