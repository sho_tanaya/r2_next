package com.d2c.bicc.util

import java.io.{File, PrintWriter}

/**
  * Created by mrikitoku on 15/12/08.
  */
object IoUtil {
  def outputAsText(text: String, fileName: String): Unit = {
    val pw = new PrintWriter(fileName)
    pw.println(text)
    pw.close()
  }

  def outputAsText(contents: Seq[String], fileName: String) : Unit = {
    val pw = new PrintWriter(fileName)
    contents.foreach {
      case text => pw.println(text)
    }
    pw.close()
  }
}