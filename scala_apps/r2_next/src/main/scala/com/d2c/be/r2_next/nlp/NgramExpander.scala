package com.d2c.be.r2_next.nlp

import scala.collection.mutable.ArrayBuffer

/**
 * Created by mrikitoku on 15/08/26.
 */
object NgramExpander {

  def allExpand(items: Seq[String], maxNgram: Int = 4): ArrayBuffer[Array[String]] = {
    val len = if (items.size < maxNgram) { items.size} else { maxNgram }
    val ngrams = ArrayBuffer.empty[Array[Int]]
    for (n <- 0 until len) {
      val ngs_n = expand(items, n, ngrams)
      ngrams ++= ngs_n
    }

    val item_ngrams = ngrams.map {
      case ng =>  ng.map {
        case pos => items(pos)
      }
    }
    item_ngrams
  }

  private def expand(items: Seq[String],
             ngram: Int = 0,
             ngrams: ArrayBuffer[Array[Int]] = ArrayBuffer.empty[Array[Int]]) : ArrayBuffer[Array[Int]] = {

    val len = items.size
    if (len == 0) {
      return ngrams
    }

    val new_ngrams = ArrayBuffer.empty[Array[Int]]
    if (ngrams.size == 0) {
      for (i <- 0 until len) {
        new_ngrams += Array(i)
      }
    } else {
      val tmp = ngrams.filter(ng => ng.size >= ngram).flatMap {
        case ngram => {
          val last = ngram.last
          for (i <- last + 1 until len) yield {
            ngram ++ Array(i)
          }
        }
      }
      new_ngrams ++= tmp
    }
    new_ngrams
  }
}
