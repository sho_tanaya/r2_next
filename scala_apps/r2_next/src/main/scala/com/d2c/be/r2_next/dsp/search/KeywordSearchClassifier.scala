package com.d2c.bicc.r2a.dsp.search

import com.d2c.be.r2_next.dsp.{Category, Keywords4Category}
import com.d2c.be.r2_next.nlp.CharacterNormalizer
import org.apache.spark.internal.Logging

/**
  * Created by mrikitoku on 16/02/24.
  */

class KeywordSearchClassifier(ckeywords: Array[Keywords4Category]) extends Serializable with Logging {
  val DELIMITER = "@"

  def categorize(query: String): Seq[(Category, Int)] = {
    //logInfo("categorize by searcher")
    var immoral = false
    val normalizedQuery = CharacterNormalizer.normalize(query, true).trim

    val result = ckeywords.map {
      case kc =>
        val c = kc.category
        val kws = kc.keywords

        val matched = kws.map {
          case kw =>
            if (kw.nonEmpty) {
              kw.split(DELIMITER).aggregate(true)((m, k) => m && normalizedQuery.indexOf(k) >= 0, (m1, m2) => m1 && m2)
            } else { false }
        }.fold(false)(_ || _)

        if (c.id == 0 && matched) {
          immoral = true
        }
        (c, if (matched) { 1 } else { -1 })
    }
    result
  }
}
