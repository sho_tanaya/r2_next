package com.d2c.be.r2_next.nlp

import org.apache.spark.internal.Logging

/**
 * Created by mrikitoku on 15/07/28.

 */
trait MorphAcceptForKeyword extends Accept with Logging {

  override
  def accept(morphSequence: String): Boolean = {
    if (morphSequence.trim.length == 0) {
      return false
    }

    val ss = morphSequence.trim.split(" ")
    // unigramの場合
    if (ss.size == 1) {
      val m = ss(0)

      // 長さ１
      try {
        if (m.split(",")(0).trim.size == 1) {
          return false
        }
      } catch {
        case e: Throwable => {
          logError("invalid input:" + m)
          System.exit(1)
        }
      }

      if (m.indexOf("接尾") >= 0) {
        return false
      }
      if (m.indexOf("接頭") >= 0) {
        return false
      }
      if (m.indexOf(",数") >= 0) {
        return false
      }
      if (m.indexOf("記号") >= 0) {
        return false
      }
    }

    // 先頭
    val start = ss(0)
    if (start.indexOf("接尾") >= 0) {
      return false
    }
    if (start.indexOf("記号") >= 0) {
      return false
    }

    // 末尾
    if (ss.size > 1) {
      val end = ss(ss.size - 1)
      if (end.indexOf("接頭") >= 0) {
        return false
      }
      if (end.indexOf("記号") >= 0) {
        return false
      }
    }

    // 数字を引き抜く
    val digit  = ss.map(m=>m.split(",")(0)).mkString("")
    !digit.matches("\\d+")
  }
}