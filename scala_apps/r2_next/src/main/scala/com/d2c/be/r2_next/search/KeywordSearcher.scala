package com.d2c.be.r2_next.search

import java.nio.file.FileSystems

import scala.collection._

import org.apache.lucene.analysis.core.SimpleAnalyzer
import org.apache.lucene.document.Document
import org.apache.lucene.index.{IndexReader, DirectoryReader}
import org.apache.lucene.queryparser.classic.QueryParser
import org.apache.lucene.search._
import org.apache.lucene.store.{Directory, FSDirectory}

import play.Logger

import com.d2c.be.r2_next.nlp.R2Tokenizer

import services.QueryNormalize

case class SearchResult(keywords: Seq[Keyword], totalHits: Int, offset: Int)

/**
 * keyword searcher
 */

object R2QueryParser {
  val dicPath = "/opt/r2dadn_server/hdic"
  val acceptPat = "名詞|接尾|接頭|数|記号|形容詞|動詞"
  val rejectPat = "副詞可能"
  val escape_charactors = Array("[", "]", "{", "}", """""")
  val tokenizer = new R2Tokenizer(dicPath, acceptPat, rejectPat)

  val OR_DELIMITER = "\\|"

  def parse(query: String) : Query = {
    // 必要があれば、形態素解析
    val tokenizedQuery = this.tokenizer.split(query).map(_.trim)
      .filter(_.nonEmpty).filter(!escape_charactors.contains(_)).filter(_ != "　").mkString(" ").trim

    if (tokenizedQuery.isEmpty) {
      null
    } else {
      var replaceQuery = tokenizedQuery.replace(" ", " AND ")
      replaceQuery = "(" + replaceQuery.split(OR_DELIMITER).mkString(") OR (") + ")"

      val qp = new QueryParser("search_target", new SimpleAnalyzer)
      val q = qp.parse(replaceQuery)
      // DEBUG
      Logger.debug("replace query: " + replaceQuery)
      Logger.debug("parse query:" + q.toString)

      q
    }
  }
}


class KeywordSearcher(indexPath: String) extends KeywordFilter with QueryNormalize {
  val SEARCH_LIMIT: Int = 100000

  private var dir: Directory = null
  private var reader: IndexReader = null
  private var searcher: IndexSearcher = null

  this.open(indexPath)

  private def open(indexPath: String): Unit = {
    this.dir = FSDirectory.open(FileSystems.getDefault().getPath(indexPath))
    this.reader = DirectoryReader.open(dir)
    this.searcher = new IndexSearcher(reader)
  }

  def update() = {
    val ndir = FSDirectory.open(FileSystems.getDefault().getPath(indexPath))
    val nreader = DirectoryReader.open(ndir)
    val nsearcher = new IndexSearcher(nreader)

    this.dir.close
    this.reader.close
    this.searcher = null

    this.dir = ndir
    this.reader = nreader
    this.searcher = nsearcher
  }

  def search(query: String, ago: Int = 30, end: Int = 0, offset: Int = 0): SearchResult = {
    val q = R2QueryParser.parse(normalizeQuery(query))
    val sr = this.doSearch(q, offset)
    // DEBUG
    if (q != null) {
      Logger.debug(s"query $query")
      Logger.debug("parse query:" + q.toString)
    } else {
      Logger.warn(s"query $query")
      Logger.warn("parse query: null")
    }
    sr
  }

  def uniqueUsers(matchedKeywords: Seq[Keyword], minSup: Int = 0) : (Int, Set[Int]) = {
    var totalFreq = 0
    val totalUids = mutable.HashSet.empty[Int]

    for (kw <- matchedKeywords) {
      Logger.debug("loop " + kw )
      // add total info
      totalUids ++= kw.user_ids
    }
    (totalFreq, totalUids.toSet)
  }


  def doSearch(query: Query, offset: Int): SearchResult = {
    val kws = mutable.ArrayBuffer.empty[Keyword]
    var totalHits = 0
    if (query != null) {
      val tds: TopDocs = this.searcher.search(query, offset + SEARCH_LIMIT)
      val hits: Array[ScoreDoc] = tds.scoreDocs
      val rangeStart = (offset) * SEARCH_LIMIT
      val rangeEnd = (offset + 1) * SEARCH_LIMIT
      for (i <- 0 until tds.totalHits.asInstanceOf[Int]) {
        if (i >= rangeStart && i < rangeEnd) {
          val sd: ScoreDoc = hits(i)
          val doc = searcher.doc(sd.doc)
          val kw = this.parseDoc(doc)
          Logger.debug("doSearch = " + kw)
          // filtering by keyword length
          if (accept(kw)) {
            kws += kw
          }
        }
      }
      totalHits = tds.totalHits.asInstanceOf[Int]
    }
    val sr = new SearchResult(kws, totalHits, offset)
    sr
  }

  def parseDoc(doc: Document) : Keyword = {
    val keyword = doc.getField("search_target").stringValue
    val user_ids_str = doc.getField("user_ids").stringValue
    // DEBUG
    Logger.debug("user_ids_str:" + user_ids_str)

    val user_ids = user_ids_str.split(" ").map(s => s.toInt)
    val kw = Keyword(keyword, user_ids)
    kw
  }
}

object KeywordSearcher {
  def main(args: Array[String]) {
    var indexPath = "./index"
    var query = ""
    var update = false
    for (i <- 0 until args.size) {
      val a = args(i)
      if (a == "-i") {
        indexPath = args(i+1)
      } else if (a == "-q") {
        query = args(i+1)
      } else if (a == "-u") {
        update = true
      }
    }

    if (query == "") {
      println("please input query string")
      sys.exit(1)
    }

    val searcher = new KeywordSearcher(indexPath)
    if (update) {
      searcher.update
    }
    val sr = searcher.search(query, 30, 0, 0)

    sr.keywords.foreach(kw => println(" " + kw.toString))
    println("#total hits:" + sr.totalHits)
    println("#offset    :" + sr.offset)
    println("#hits:")
  }
}
