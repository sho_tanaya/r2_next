package com.d2c.be.r2_next.nlp

import java.util

import com.rikima.ml.{Hashing, LabeledFeatureVector, Feature}

import scala.collection.mutable

/**
  * Created by masakir on 2015/11/15.
  */
trait FeatureVectorMaker extends CharacterNormalize {
  val SPACE = "　"

  val dicPath = "/opt/igo-xi/hdic"
  val acceptPat = "名詞|接尾|接頭|数|記号|形容詞"
  //val acceptPat = "名詞|形容詞|動詞"
  val rejectPat = "副詞可能|地名"

  private val tokenizer = new R2Tokenizer(dicPath, acceptPat, rejectPat)

  def toFeatureVector(text: String, y: Int = 0): LabeledFeatureVector = {
    val buffer: util.TreeSet[Feature] = new util.TreeSet()
    for (s <- this.parse(normalize(text, true))) {
      val feature = s
      // hashing trick
      val fid = Hashing.hash(feature, 1, 1 << 20)
      val f = new Feature(fid, 1.0)
      buffer.add(f)
    }
    val lfv: LabeledFeatureVector = new LabeledFeatureVector(y, buffer)
    lfv
  }

  def toBigramFeatureVector(text: String, y: Int = 0): LabeledFeatureVector = {
    val buffer: util.TreeSet[Feature] = new util.TreeSet()
    var prev = SPACE
    for (s <- this.parse(normalize(text, true))) {
      val feature = prev + s
      // hashing trick
      val fid = Hashing.hash(feature, 1, 1 << 20)
      val f = new Feature(fid, 1.0)
      buffer.add(f)
      prev = s
    }
    val lfv: LabeledFeatureVector = new LabeledFeatureVector(y, buffer)
    lfv
  }

  def parse(text: String): Seq[String] = {
    // morph analyze only
    tokenizer.split(text)
  }
}
