package com.d2c.be.r2_next.adn

import com.d2c.bicc.util.DateUtil
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.internal.Logging

/**
 * Created by mrikitoku on 15/07/28.
 */

object FrequentKeywordAggregator extends Logging {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName(s"FrequentKeywordAggregator")

    var output_prefix = "hdfs://r2a001:9000/data/dadn/frequent_keywords/"
    var input_prefix = "hdfs://r2a001:9000/data/dadn/query/"

    var ver = ""
    var min_dau = 2
    var min_mau = 100
    var days = 30
    for (i <- 0 until args.size) {
      val a = args(i)
      if (a == "-v" || a == "--ver") {
        ver = args(i + 1)
      } else if (a == "-md" || a == "--min_dau") {
        min_dau = args(i + 1).toInt
      } else if (a == "-ma" || a == "--min_mau") {
        min_mau = args(i + 1).toInt
      } else if (a == "-m" || a == "--memory") {
        conf.set("spark.executor.memory", args(i + 1))
      }
      if (a == "-i" || a == "--input_prefix") {
        input_prefix = args(i+1)
      }
      if (a == "-o" || a == "--output_prefix") {
        output_prefix = args(i+1)
      }
      if (a == "-d" || a == "--days_ago") {
        days = args(i+1).toInt
      }
    }
    val sc = new SparkContext(conf)

    if (ver.isEmpty) {
      println("please input end ver string")
      System.exit(1)
    }

    val query_processor = new DadnQuery(input_prefix)

    // save unique users
    val mau: RDD[((String, Option[String], Option[String], Option[String]), Long)] = query_processor.monthlyUU(sc, ver, days)
    mau.map {
      case ((segId, f81Id, appId, idfa), userId) => Array(userId, segId, f81Id.getOrElse("none"), appId.getOrElse("none"), idfa.getOrElse("none")).mkString("\t")
    }.saveAsTextFile(s"$output_prefix/$ver/mau2")

    var keyword2users: RDD[(String, (Int, Set[String]))] = sc.emptyRDD[(String, (Int, Set[String]))]

    for (ago <- 0 until days) {
      try {
        val date = DateUtil.agoDate(ver, ago)
        val k2u: RDD[(String, (Int, Set[String]))] = sc.textFile(s"$output_prefix/$date/keyword2userids2").map(l => l.split("\t")).map {
          case ss => (ss(0), (ss(1).toInt, ss(2).split(" ").toSet))
        }
        keyword2users = keyword2users.union(k2u)
      } catch {
        case e: Throwable => {
          logError("error $ago")
        }
      }
    }
    // log
    logInfo(s"#keywords2users: " + keyword2users.count)

    // aggregate past 30 days results
    val frequent_keywords_with_users = aggregate(keyword2users, min_mau)

    // collect user_ids


    //val cookie_id2long_ids: Map[String, Long] = mau.map(x => (x._1._1, x._2)).collect.toMap
    val cookieId2longIdMap = mau.groupBy(_._1._1).mapValues(_.map(_._2)).collect.toMap

    // save results
    frequent_keywords_with_users.map {
      case (kw, (freq, uids)) => {
        val luids = uids.flatMap(c => cookieId2longIdMap.getOrElse(c, Seq(0L)))
        Array(kw, freq.toString, luids.mkString(" ")).mkString("\t")
      }
    }.saveAsTextFile(s"$output_prefix/$ver/frequent_keywords2.tsv")
  }

  def aggregate(keyword2users: RDD[(String, (Int, Set[String]))], min_mau: Int = 100): RDD[(String, (Int, Set[String]))] = {
    keyword2users.aggregateByKey((0, Set.empty[String]))(
      seqOp = (u, v) => (u._1 + v._1, u._2 ++ v._2),
      combOp = (u1, u2) => (u1._1 + u2._1, u1._2 ++ u2._2)
    ).filter { case (k, v) => v._2.size > min_mau}
  }
}
