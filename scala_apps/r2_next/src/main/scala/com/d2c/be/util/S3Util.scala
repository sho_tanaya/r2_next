package com.d2c.bicc.util

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
 * Created by mrikitoku on 15/07/23.
 */
class S3Util(bucket: String = "d2c-r2a-infra") {

  def loadText(sc: SparkContext, fname: String) : RDD[String] = {
    sc.textFile(getUrl(fname))
  }

  def saveAsTsv(fname: String, data: RDD[Seq[String]]) {
    val sc = data.context
    data.map(a => a.mkString("\t")).saveAsTextFile(this.getUrl(fname))
  }

  def getUrl(fname: String) = {
    s"s3n://${bucket}/${fname}"
  }
}
