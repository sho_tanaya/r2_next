package com.d2c.bicc.r2a.dsp.classifier

import java.io.{File, FileFilter}

import com.d2c.be.r2_next.dsp.{DspSegmentUtil, Category}
import com.d2c.be.r2_next.nlp.FeatureVectorMaker
import com.rikima.ml.Model
import com.rikima.ml.oll.OLLClassifier

import scala.collection.mutable
import scala.io.Source

/**
  * Created by masakir on 2015/11/15.
  */

class QueryClassifier() extends Serializable with FeatureVectorMaker with DspSegmentUtil {
  private var binaryClassifiers: mutable.Map[Category, OLLClassifier] = mutable.Map.empty[Category, OLLClassifier]


  def loadFromLocalDir(modelDir: String) = {
    val dir = new File(modelDir)
    dir.listFiles().filter(_.isDirectory).map {
      case d =>
        val cid = d.getName.toInt
        val mf = d.getAbsolutePath + "/" + MODEL_FILE_NAME
        println(s"model file: $mf")
        val json = Source.fromFile(mf).getLines().mkString("")
        try {
          val model = Model.construct(json)
          val c = new OLLClassifier(model)
          val cat = Category(model.id, model.name)
          val name = model.name
          println(s"classifier for $cid $name")
          this.binaryClassifiers.put(cat, c)
        } catch {
          case e: Throwable => e.printStackTrace()
        }
    }
  }


  def categorize(query: String): Seq[(Category, Int, Double)] = {
    val result = this.binaryClassifiers.map {
      case (category, classifier) => {
        //val fv = toFeatureVector(query)
        val fv = toBigramFeatureVector(query)
        val y = classifier.classify(fv)
        val score = classifier.score(fv)

        (category, y, score)
      }
    }
    result.toSeq
  }
}
