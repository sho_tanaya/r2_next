package com.d2c.be.r2_next.adn

import com.d2c.be.r2_next.nlp.{CharacterNormalizer, MorphAcceptForKeyword, Query, R2Tokenizer}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.internal.Logging

import scala.collection.mutable.ArrayBuffer

/**
 * Created by mrikitoku on 15/07/28.
 */

object DailyFrequentKeywordExtracter extends MorphAcceptForKeyword with Logging {
  val TOKEN_DELIMITER = "　"
  val MORPH_DELIMITER = " "

  val acceptPat = "名詞|接尾|接頭|数|記号|形容詞|動詞"
  val rejectPat = "副詞可能|空白"

  val dicPath = "/opt/igo-xi/hdic"

  val numPartitions = 100

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName(s"Onpure FrequentKeywordExtracter")

    var output_prefix = s"hdfs://r2a001:9000/data/dadn/frequent_keywords/"
    var input_prefix = s"hdfs://r2a001:9000/data/dadn/query/"

    var ver = ""
    var minDau = 2
    var minMau = 100
    for (i <- 0 until args.size) {
      val a = args(i)
      if (a == "-v" || a == "--ver") {
        ver = args(i + 1)
      } else if (a == "-md" || a == "--min_dau") {
        minDau = args(i + 1).toInt
      } else if (a == "-ma" || a == "--min_mau") {
        minMau = args(i + 1).toInt
      }
      if (a == "-i" || a == "--input_prefix") {
        input_prefix = args(i+1)
      }
      if (a == "-o" || a == "--output_prefix") {
        output_prefix = args(i+1)
      }
    }
    val sc = new SparkContext(conf)

    if (ver.isEmpty) {
      println("please input end ver string")
      System.exit(1)
    }

    val query_processor = new DadnQuery(input_prefix)
    val queries = query_processor.asDadnQueryRow(sc, ver)

    val formatedKeywords = mine(queries, minDau)
    val url = s"$output_prefix/$ver/keyword2userids2"

    formatedKeywords.map {
      case (kw, freq, uids) =>
        Array(kw, freq.toString, uids.mkString(" ")).mkString("\t")
    }.saveAsTextFile(url)
  }

  /**
    * mine
    * @param input
    * @param minSup
    * @return
    */
  def mine(input: RDD[DadnQueryRow], minSup: Int = 1) : RDD[(String, Int, Set[String])] = {
    val tokenizer = new R2Tokenizer(dicPath, acceptPat, rejectPat)

    //並び替え
    //しない
    val queryRdd: RDD[(Query, ArrayBuffer[String])] = input.map {
      case unit => (Query(normalize(unit.keyword)).parse(tokenizer), unit.user_id)
    }.aggregateByKey(ArrayBuffer.empty[String]) (
      seqOp = (u, v) => u += v,
      combOp = (u1, u2) => u1 ++ u2
    )
    //queryRdd.repartition(1000)
    queryRdd.cache

    val ngramRdd = queryRdd.flatMap {
      case (query: Query, userIds) => {
        query.getNgrams.map(ng => (ng, userIds))
    }
    }.aggregateByKey(ArrayBuffer.empty[String])(
      seqOp = (u, v) => u ++ v,
      combOp = (u1, u2) => (u1 ++ u2)
    )//.repartition(1000)

    // 集計
    val frequentKeywords = ngramRdd.filter {
      case (ngram, uids) => uids.distinct.size > minSup
    }.map {
      case (kw, uids) => (kw, uids.size, uids.distinct.toSet)
    }
    //queryRdd.unpersist(false)
    frequentKeywords
  }


  def normalize(text: String) : String = {
    CharacterNormalizer.normalize(text, true)
  }

}
