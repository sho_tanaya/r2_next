package com.d2c.be.r2_next.search

/**
 * Created by mrikitoku on 15/09/02.
 */
trait KeywordFilter {
  val MAX_LENGTH = 255

  def accept(kw: Keyword) : Boolean = {
    if (kw.keyword.trim.size > MAX_LENGTH) {
      return false
    }
    true
  }
}
