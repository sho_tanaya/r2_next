package com.d2c.be.r2_next.adn

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.internal.Logging
/**
 * Created by mrikitoku on 15/07/29.
 */

object DadnUserSegmentMaker extends Logging {
  val bucket = "s3n://d2c-r2a-infra"
  val prefix = "/dsp/query/dsp_query"
  val delimiter = "\t"

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName(s"DadnSegmentMaker")
    val days = 30

    var date = ""
    val output_prefix = s"s3n://d2c-r2a-infra/dsp/frequent_keywords/"
    for (i <- 0 until args.size) {
      val a = args(i)
      if (a == "-d" || a == "--date") {
        date = args(i + 1)
      } else if (a == "-m" || a == "--memory") {
        conf.set("spark.executor.memory", args(i + 1))
      }
    }
    val sc = new SparkContext(conf)

    if (date.trim.size == 0) {
      println("please input target ver string")
      System.exit(1)
    }

    // optout user
    val outouts = sc.textFile(s"./data/optout/$date/*")

    // users
    //val mau = DadnQuery.monthlyUU(sc, date, days)

    // frequent users
    /*
    for (ago <- 0 until days) {
      try {
        val target = DateUtil.agoDate(date, ago)
        val rows = DadnQuery.asDspQueryRow(sc, date, 0)
        val dspQueries = DadnQuery.asDspQueryUnit(sc, rows, mau)

        val formatedKeywords = mine(dspQueries, numPartitions, memory)
        val url = "%s/%s/keywords_%02d".format(output_prefix, date, ago)
        // output to s3
        formatedKeywords.map { case (kw, freq, uids) => s"$kw\t$freq\t" + uids.mkString("\t") }.saveAsTextFile(url)
      } catch {
        case e: Throwable => {
          logError("error $ago")
        }
      }
    }
    */
  }
}

