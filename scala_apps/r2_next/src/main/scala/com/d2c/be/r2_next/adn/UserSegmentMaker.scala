package com.d2c.be.r2_next.adn

import com.d2c.be.r2_next.search.R2QueryParser.OR_DELIMITER
import com.d2c.be.r2_next.search.KeywordSearcher
import play.api.Logger

import scala.io.Source
import scala.collection.mutable

import java.io.PrintWriter
import play.api.libs.json.Json
import services.SegmentLevel


/**
  * Created by mrikitoku on 15/09/15.
  */

case class Segment(id: Int, queries: Seq[String]) {
  def mkQuery: String = {
    queries.map(q => q.trim).mkString(OR_DELIMITER)
  }
}

case class SegmentRequest(segments: Seq[Segment])

object Segment {
  //implicit def jsonWrites = Json.writes[University]
  implicit def jsonReads = Json.reads[Segment]
}

object SegmentRequest {
  //implicit def jsonWrites = Json.writes[Profile]
  implicit def jsonReads = Json.reads[SegmentRequest]
}


object UserSegmentMaker extends SegmentLevel {
  val DEFAULT_INDEX = "./index"
  var index_path = DEFAULT_INDEX
  var searcher: KeywordSearcher = null

  def main(args: Array[String]) = {
    var optoutUserFile = ""
    var requestFile = ""
    var outputFile = ""
    var appOutputFile = ""
    var f81idTable = ""
    var uuOutput = ""
    for (i <- 0 until args.size) {
      if (args(i) == "-o" || args(i) == "--optout") {
        optoutUserFile = args(i + 1)
      } else if (args(i) == "-r" || args(i) == "--request") {
        requestFile = args(i + 1)
      } else if (args(i) == "-f" || args(i) == "--f81id_table") {
        f81idTable = args(i + 1)
      } else if (args(i) == "-op" || args(i) == "--output") {
        outputFile = args(i + 1)
      } else if (args(i) == "-aop" || args(i) == "--app_output") {
        appOutputFile = args(i + 1)
      } else if (args(i) == "-u" || args(i) == "--uu") {
        uuOutput = args(i + 1)
      }
      if (args(i) == "-i" || args(i) == "--index") {
        index_path = args(i + 1)
      }
    }

    searcher = new KeywordSearcher(index_path)

    // optout users
    var optoutUsers: Set[String] = null
    try {
      optoutUsers = loadOptoutUsers(optoutUserFile)
    } catch {
      case e: Throwable => Logger.error("failed to load optout users", e)
    }

    // request file
    var request: SegmentRequest = null
    try {
      request = readSegmentRequest(requestFile)
    } catch {
      case e: Throwable => Logger.error("failed to load segment request", e)
    }

    // f81 user ids
    var uid2f81Ids: mutable.Map[Int, (Option[String], Option[String])] = null
    try {
      uid2f81Ids = readF81Table(f81idTable)
    } catch {
      case e: Throwable => Logger.error("failed to load f81 user id tables", e)
    }

    val output = if (outputFile.length > 0) {
      outputFile
    } else {
      s"$requestFile.segments"
    }

    val appOutput = if (appOutputFile.length > 0) {
      appOutputFile
    } else {
      s"$requestFile.segments_app"
    }

    val pw = new PrintWriter(output)
    val apw = new PrintWriter(appOutput)
    val uuPw = new PrintWriter(uuOutput)
    try {
      run(optoutUsers, request, uid2f81Ids, pw, apw, uuPw)
    } catch {
      case e: Throwable => Logger.error("", e)
    }
    finally {
      pw.close
      apw.close
      uuPw.close
    }
  }

  def readF81Table(file: String): mutable.Map[Int, (Option[String], Option[String])] = {
    Logger.info("read f81 userids")
    val io = Source.fromFile(file)
    // long id , f81 id
    val map = mutable.HashMap.empty[Int, (Option[String], Option[String])]
    for (l <- io.getLines) {
      val ss = l.split("\\s", -1) //空文字を省略させないため

      val id = ss(0).toInt
      //val segId = ss(1)
      val f81id = if (ss.length > 2 && !ss(2).isEmpty && ss(2) != "none") Some(ss(2)) else None
      val appId = if (ss.length > 3 && !ss(3).isEmpty && ss(3) != "none") Some(ss(3)) else None
      map.put(id, (f81id, appId))
    }
    Logger.info(s"f81 userids size ${map.size}")
    map.keys.take(3).map(x => map.get(x)).flatMap(x => x).foreach(value => {
      Logger.info(s"f81 userid ${value._1.getOrElse("none")} ${value._2.getOrElse("none")}")
    })

    map
  }

  def loadOptoutUsers(file: String): Set[String] = {
    Logger.info(" load optout users")
    val io = Source.fromFile(file)
    val set = io.getLines().toArray.map(l => l.trim).toSet
    Logger.info(s" load optout users size ${set.size}")
    Logger.info(" .done")
    set
  }

  def readSegmentRequest(file: String): SegmentRequest = {
    Logger.info(" load segment request ...")
    val io = Source.fromFile(file)
    val json = io.getLines().mkString("")
    // DEBUG
    Logger.debug(json)
    val request = Json.parse(json).validate[SegmentRequest]
    Logger.info(" .done")
    request.get
  }

  def run(optoutUsers: Set[String], request: SegmentRequest, uid2f81Ids: mutable.Map[Int, (Option[String], Option[String])],
          pw: PrintWriter, apw: PrintWriter, uuPw: PrintWriter,
          minSup: Int = 100) = {
    // DEBUG
    Logger.debug("----")
    Logger.debug(s"request.segments")

    val userId2segId = mutable.HashMap.empty[String, mutable.HashSet[Int]]
    val appId2SegId = mutable.HashMap.empty[String, mutable.HashSet[Int]]
    val uniqueUsers = mutable.HashSet.empty[Int]

    // aggregation
    request.segments.foreach {
      case segment => {
        val segId = segment.id
        // DEBUG
        Logger.debug(" segment query:" + segment.mkQuery)
        uniqueUsers.clear()
        segment.queries.foreach {
          case query => {
            try {
              val sr = searcher.search(query)
              Logger.debug("#total hits:" + sr.totalHits + " " + sr.keywords.size)
              sr.keywords.foreach {
                case kw => {
                  // DEBUG
                  Logger.debug(" kw:" + kw.keyword + " kw.user_ids:" + kw.user_ids)
                  uniqueUsers ++= kw.user_ids
                }
              }
            } catch {
              case e: Throwable => Logger.error(s"wrong query: $query", e)
            }
          }
        }
        val uu = uniqueUsers.size
        val level = setLevel(uu)
        // output uu file
        uuPw.println(s"$segId\t$uu\t$level")


        if(uu >= minSup){
          // segment id出力
          uniqueUsers.foreach {
            case uid: Int => {
              val ids: Option[(Option[String], Option[String])] = uid2f81Ids.get(uid)

              def mapping(id: String, segMap: mutable.HashMap[String, mutable.HashSet[Int]], optoutUsers: Set[String] = Set()) = {
                if (!optoutUsers.contains(id)) {
                  if (!segMap.contains(id)) {
                    segMap.put(id, mutable.HashSet.empty[Int])
                  }
                  if (check(id)) {
                    segMap.get(id).get.add(segId)
                  }
                }
              }


              ids match {
                case Some((Some(fid), Some(aid))) => {
                  mapping(fid, userId2segId, optoutUsers)
                  mapping(aid, appId2SegId)
                }
                case Some((Some(fid), None)) => {
                  mapping(fid, userId2segId, optoutUsers)
                }
                case Some((None, Some(aid))) => {
                  mapping(aid, appId2SegId)
                }
                case _ => Unit
              }
            }
          }
        }
      }
    }
    // output
    for ((userId, segIds) <- userId2segId.iterator) {
      if (check(userId)) {
        pw.print(userId)

        segIds.foreach {
          case segId => pw.print(s"\t$segId")
        }
        pw.println
      }
    }

    for ((appId, segIds) <- appId2SegId.iterator) {
      if (check(appId)) {
        apw.print(appId)
        segIds.foreach {
          case segId => apw.print(s"\t$segId")
        }
        apw.println
      }
    }
  }

  def check(userId: String): Boolean = {
    userId != null && !userId.trim.toLowerCase.equals("null")
  }
}
