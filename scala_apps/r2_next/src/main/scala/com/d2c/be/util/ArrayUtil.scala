package com.d2c.bicc.util

/**
 * Created by mrikitoku on 15/07/30.
 */
object ArrayUtil {
  def normalize(array: Array[Double]) : Array[Double] = {
    val sum = array.sum
    if (sum != 0) { array.map(v => v / sum) } else { array }
  }

  def times(array: Array[Double], coef: Double) : Array[Double] = {
    array.map(v => v * coef)
  }
}
