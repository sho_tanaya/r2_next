package com.d2c.bicc.r2a.dsp.classifier

import java.io.File

import com.d2c.be.r2_next.dsp.{DspSegmentUtil, Category, Examples4Category}
import com.d2c.be.r2_next.nlp.FeatureVectorMaker

import com.rikima.ml.Model
import com.rikima.ml.oll.OLLTrainerFactory.Algorithm
import com.rikima.ml.oll.{OLLTrainer, OLLTrainerFactory}

/**
  * Created by mrikitoku on 15/12/08.
  */

object QueryClassifierTrainer extends FeatureVectorMaker with DspSegmentUtil {
  val COMMENT_MARK = "#"
  val NEGATIVE_MARK = "-"
  val IMMORAL_MARK = "\\"

  val alg = Algorithm.SCW
  val c = 1.0
  val bias = 0


  def trainAll(trainers: Seq[(Category, OLLTrainer)], trainData: Seq[Examples4Category], tryCount: Int = 50): Seq[(Category, Model)] = {
    trainers.map {
      case (category, trainer) =>
        (category, train(category, trainer, trainData, tryCount))
    }
  }


  def train(category: Category, trainer: OLLTrainer, trainData: Seq[Examples4Category], tryCount: Int = 50) : Model = {
    var pp = 0
    var pn = 0
    var np = 0
    var nn = 0

    var i = 0

    trainData.foreach {
      case example =>
        val c = example.category
        val exs = example.examples
        exs.map(_.trim)
          .filter(!_.startsWith(COMMENT_MARK))
          .filter( _.nonEmpty)
          //.filter(!_.startsWith(NEGATIVE_MARK))
          .foreach {
          case ex => {
            val y_1 = if (c.id == category.id) 1 else -1
            //val (y_2, e) = if (ex.startsWith(NEGATIVE_MARK) || ex.startsWith(IMMORAL_MARK)) { (-1, ex.substring(1)) } else { (1, ex) }
            val (y_2, e) =
              if (ex.startsWith(IMMORAL_MARK)) {
                if (c.id == 0) {
                  (1, ex.substring(1))
                } else {
                  (-1, ex.substring(1))
                }
              } else if (ex.startsWith(NEGATIVE_MARK)) {
                (-1, ex.substring(1))
              } else {
                (1, ex)
              }

            val y = if (y_1 < 0 || y_2 < 0) -1  else 1

            //val lfv = toFeatureVector(e, y)
            val lfv = toBigramFeatureVector(e, y)

            var exec = true
            var limit = tryCount
            var v = 0.0
            while (exec) {
              trainer.trainExample(lfv, i)
              v = trainer.getMargin(lfv)
              if (v > 0) {
                exec = false
              }
              limit -= 1
              if (limit < 0) {
                exec = false
              }
              println(s"y=$y margin:$v query:$e" )
            }
            v = trainer.getClassifier.score(lfv)
            if (y > 0) {
              if (v > 0) {
                pp += 1
              } else {
                pn += 1
                //println(s"y=$y l:$l" )
              }
            } else {
              if (v > 0) {
                np += 1
                //println(s"y=$y l:$l" )
              } else {
                nn += 1
              }
            }

            i += 1
          }
        }
    }

    // printout
    println(s"%% category id: ${category.id} ${category.name}")
    println(s"%% pp:$pp pn:$pn np:$np nn:$nn")
    trainer.getModel
  }



  /**
    * main
    *
    * @param args
    */
  def main(args: Array[String]): Unit = {
    var data_dir: Option[File] = None
    var model_dir: Option[File] = None
    for (i <- 0 until args.length) {
      val a = args(i)
      if (a == "-m" || a == "--model_dir") {
        model_dir = Some(new File(args(i+1)))
      }
      if (a == "-d" || a == "--data_dir") {
        data_dir = Some(new File(args(i+1)))
      }
    }

    if (model_dir.isEmpty || data_dir.isEmpty) {
      println("please input model_dir and data_dir")
      System.exit(1)
    }

    println(s"data_dir: $data_dir")
    println(s"model_dir: $model_dir")

    // construct trainers
    val trainers = construct_trainers(model_dir.get)

    // load examples
    val examples = load_examples(data_dir.get)

    val models = trainAll(trainers, examples)

    // output models
    output_models(model_dir.get, models)
  }
}
