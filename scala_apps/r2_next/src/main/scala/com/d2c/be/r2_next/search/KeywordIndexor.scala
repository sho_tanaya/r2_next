package com.d2c.be.r2_next.search

import java.nio.file._

import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.core.SimpleAnalyzer
import org.apache.lucene.document._
import org.apache.lucene.index.IndexWriterConfig.OpenMode
import org.apache.lucene.index.{IndexWriter, IndexWriterConfig, Term}
import org.apache.lucene.store.{Directory, FSDirectory}
import play.Logger

import scala.io.Source


class KeywordIndexor (indexPath: String) {
  val dir: Directory = FSDirectory.open(FileSystems.getDefault().getPath(indexPath))

  val analyzer: Analyzer = new SimpleAnalyzer

  val iwc: IndexWriterConfig = new IndexWriterConfig(analyzer).setOpenMode(OpenMode.CREATE_OR_APPEND)
  val writer: IndexWriter = new IndexWriter(dir, iwc)

  def indexing(doc: Document): Unit = {
    writer.addDocument(doc)
  }

  def createDocument(kw: Keyword): Document = {
    val doc: Document = new Document
    doc.add(new TextField("search_target", kw.keyword, Field.Store.YES))
    doc.add(new Field("user_ids", kw.stringUserIds, TextField.TYPE_STORED))

    doc
  }

  def terminate {
    writer.commit
    writer.close
    dir.close
  }

  def allDelete(): Unit = {
    this.writer.deleteAll()
  }
}

object KeywordIndexor {
  def main(args: Array[String]): Unit = {
    var indexPath: String = "index"
    var tsv: String = null
    var allDelete = false
    for (i <- 0 until args.size) {
      if ("-index" == args(i)) {
        indexPath = args(i + 1)
      } else if ("-i" == args(i)) {
        tsv = args(i + 1)
      } else if ("--all_delete" == args(i)) {
        allDelete = true
      }
    }

    if (tsv == null) {
      println("KeywordIndexor -i [input tsv ] --ver [date] (-index [index path ], --delete_ver) ")
      System.exit(1)
    }

    val indexor: KeywordIndexor = new KeywordIndexor(indexPath)
    // clear document for ver
    if (allDelete) {
      indexor.allDelete
    }

    val io = Source.fromFile(tsv)
    var i = 0
    for (l <- io.getLines) {
      val ss = l.trim.split("\t")
      val keyword = ss(0)
      val user_ids_str = ss.slice(1, ss.length).mkString(" ")

      val user_ids = user_ids_str.split(" ").map(t => t.trim).filter(t => t.length > 0).map(d => d.toInt)
      val duu = user_ids.size
      i += 1
      if (i % 10000 == 0) {
        print(".")
      }

      val doc = indexor.createDocument(Keyword(keyword, user_ids))
      indexor.indexing(doc)
    }
    indexor.terminate
  }
}
