package com.d2c.be.r2_next.dsp

import java.io.File

import com.d2c.bicc.util.IoUtil
import com.rikima.ml.Model
import com.rikima.ml.oll.OLLTrainerFactory.Algorithm
import com.rikima.ml.oll.{OLLTrainerFactory, OLLTrainer}

import org.apache.spark.SparkContext
import org.apache.spark.rdd._

import scala.io.Source
import scala.util.Random

/**
  * Created by mrikitoku on 16/02/26.
  */

case class Keywords4Category(category: Category, keywords: Array[String])

case class Query(text: String, userId: String)

case class Examples4Category(category: Category, examples: Seq[String])

case class Category(id: Int, name: String)

trait DspSegmentUtil {
  val MODEL_FILE_NAME = "r2dsp.SCW.model.json"

  def load_examples(dir: File): Seq[Examples4Category] = {
    println(dir)
    val data = dir.listFiles().filter(_.isDirectory).map {
      case d =>
        println(d)
        val cid = d.getName.toInt
        val fname = d.getAbsolutePath + "/examples.txt"
        if (new File(fname).exists()) {
          val kws = Source.fromFile(fname).getLines
          println(cid, fname)
          Examples4Category(Category(cid, cid.toString), kws.toList)
        } else {
          Examples4Category(Category(cid, cid.toString), Array.empty[String])
        }
    }
    val rand = new Random
    rand.shuffle(data)
  }

  def construct_trainers(model_dir: File, alg: Algorithm = Algorithm.SCW, c: Double = 1.0, bias: Double = 0.0): Seq[(Category, OLLTrainer)] = {
    model_dir.listFiles().filter(_.isDirectory).map {
      case d =>
        println(d)
        val cid = d.getName.toInt
        val modelFile = new File(d.getAbsolutePath + "/r2dsp.SCW.model.json")
        val trainer: OLLTrainer = OLLTrainerFactory.create(alg, c, bias)

        val json = Source.fromFile(modelFile).getLines.mkString("")
        trainer.setModel(Model.construct(json))
        println(cid, json)

        (Category(cid, cid.toString), trainer)
    }
  }


  def output_models(model_dir: File, models: Seq[(Category, Model)]) {
    models.foreach {
      case (category, model) =>
        val cid = category.id
        model.setId(cid)
        model.setName(category.name)
        val json = model.toJson
        val modelFile = new File(model_dir.getAbsolutePath + s"/$cid/r2dsp.SCW.model.json.trained")
        IoUtil.outputAsText(json, modelFile.getAbsolutePath)
    }
  }

  //def getQueryByDate(sc: SparkContext, date: String, prefix: String = "/user/r2a/data/dsp/query/", num_partitions: Int = 100): RDD[Query] = {
  def getQueryByDate(sc: SparkContext, date: String, prefix: String = "hdfs://192.168.190.200:9000/data/dsp/query/", num_partitions: Int = 100): RDD[Query] = {
    sc.textFile(s"$prefix/${date}/dsp_query_${date}.tsv", num_partitions)
      .map(l => l.split("\t")).map(ss => Query(ss(0), ss(1)))
  }

  def getQuery(sc: SparkContext, input_path: String,  num_partitions: Int = 20): RDD[Query] = {
    sc.textFile(s"$input_path", num_partitions)
      .map(l => l.split("\t")).map(ss => Query(ss(0), ss(1)))
  }
}

