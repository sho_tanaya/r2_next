package com.d2c.be.r2_next.nlp

import net.reduls.igo.{Morpheme, Tagger}

import scala.collection.mutable

import net.reduls.igo.dictionary.factory.Type

/**
 *
 * Created by mrikitoku on 15/01/27.
 */
class R2Tokenizer(dicPath: String, acceptRegex:String, rejectRegex: String ) {
  val space = "　"
  var tagger: Tagger = if (dicPath.trim.startsWith("hdfs://")) { new Tagger(dicPath, Type.HDFS) } else { new Tagger(dicPath, Type.ON_HEAP) }

  val acceptPat = acceptRegex.r
  val rejectPat = rejectRegex.r

  def parse(text: String): Seq[String] = {
    var morphs = mutable.ArrayBuffer.empty[String]
    val ms = tagger.parse(text)
    for (i <- 0 until ms.size) {
      val m = ms.get(i)
      if (rejectRegex.length == 0 || !rejectPat.findAllIn(m.feature).hasNext) {
        // accept pattern
        if (acceptRegex.length > 0) {
          if (acceptPat.findAllIn(m.feature).hasNext) {
            //if (m.feature.indexOf(acceptRegex) >= 0) {
            morphs = morphs :+ Array(m.surface, m.className, m.dClassName).mkString(",")
          } else {
            // skipt morpheme
            morphs = this.addDelimiter(morphs)
          }
        } else {
          morphs = morphs :+ m.surface + "," + Array(m.surface, m.className, m.dClassName).mkString(",")
        }
      } else {
        morphs = this.addDelimiter(morphs)
      }
    }
    morphs
  }

  def split(text: String): Seq[String] = {
    var morphs = mutable.ArrayBuffer.empty[String]
    val ms = tagger.parse(text)
    for (i <- 0 until ms.size) {
      val m = ms.get(i)
      if (rejectRegex.length == 0 || !rejectPat.findAllIn(m.feature).hasNext) {
        // accept pattern
        if (acceptRegex.length > 0) {
          if (acceptPat.findAllIn(m.feature).hasNext) {
            //if (m.feature.indexOf(acceptRegex) >= 0) {
            morphs = morphs :+ m.surface
          } else {
            // skipt morpheme
            morphs = this.addDelimiter(morphs)
          }
        } else {
          morphs = morphs :+ m.surface
        }
      } else {
        morphs = this.addDelimiter(morphs)
      }
    }
    morphs
  }

  private def addDelimiter(morphs: mutable.ArrayBuffer[String]): mutable.ArrayBuffer[String] = {
    val len = morphs.size
    if (len > 0 && morphs(len-1) != space) {
      // skip morpheme
      morphs :+ space
    } else {
      morphs
    }
  }
}
