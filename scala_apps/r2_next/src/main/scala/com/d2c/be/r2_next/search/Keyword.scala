package com.d2c.be.r2_next.search

/**
 * Created by mrikitoku on 15/08/24.
 */
case class Keyword(keyword: String, user_ids: Seq[Int]) {
  def uu : Int = {
    this.user_ids.size
  }

  def stringUserIds : String = {
    user_ids.map(id => id.toString).mkString(" ")
  }

  override def toString: String = {
    val s = if (this.user_ids == null) {""} else {this.user_ids.mkString(" ")}
    s"keyword: $keyword user_ids: $s"
  }
}
