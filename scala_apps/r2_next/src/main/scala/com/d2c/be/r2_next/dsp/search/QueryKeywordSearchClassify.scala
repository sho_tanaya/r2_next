package com.d2c.bicc.r2a.dsp.search

import java.io.File

import com.d2c.be.r2_next.dsp.{Category, DspSegmentUtil, Keywords4Category, Query}
import com.d2c.be.r2_next.nlp.CharacterNormalizer
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.io.Source

/**
  * Created by masakir on 2015/11/15.
  */
object QueryKeywordSearchClassify extends DspSegmentUtil {
  val COMMENT = "#"

  /**
    * categorize
    *
    * @param classifier
    * @param queries
    * @return
    */
  def categorize(classifier: KeywordSearchClassifier, queries: RDD[Query]): RDD[(String, Category, Int, String)] = {
    val bcClassifier = queries.context.broadcast(classifier)

    val categorized = queries.flatMap {
      case q => {
        val query = q.text
        val userId = q.userId

        val predict = bcClassifier.value.categorize(query)
        // immoral check
        val (cat0, y) = predict.filter {
          case (c, y) => c.id == 0
        }(0)
        if (y > 0) {
          //predict.map { case (category, y) => (userId, category, y, query) }
          None
        } else {
          predict.map { case (category, y) => (userId, category, y, query) }
        }
      }
    }.filter {
      case (userId, category, y, text) => (y > 0)
    }.filter {
      case (userId, category, y, text) => category.id > 0
    }.filter {
      case (userId, category, y, text) => userId != "NULL"
    }
    categorized
  }

  def load_keywords(dir: File): Array[Keywords4Category] = {
    dir.listFiles().filter(_.isDirectory).map {
      case d =>
        println(d)
        val cid = d.getName.toInt
        val fname = d.getAbsolutePath + "/keywords.txt"
        val kws = Source.fromFile(fname).getLines.map(_.trim).filter(!_.startsWith(COMMENT)).map {
          case l =>
            val p = l.indexOf(COMMENT)
            if (p > 0) {
              l.substring(0, p)
            } else {
              l
            }
        }.map(x => CharacterNormalizer.normalize(x, true).trim)
        println(cid, fname)

        Keywords4Category(Category(cid, cid.toString), kws.toArray)
    }
  }


  def format4dsp(date: String, categorized: RDD[(String, Category, Int, String)]): RDD[String] = {
    val formatted = categorized.map {
      case (userId, category, y, query) => {
        val cid = category.id
        Array(date, s"""{"aid": "d2c", "sid": "DCMkensakucategory", "uid": "$userId", "category$cid": 1 }""", query, category.id).mkString("\t")
      }
    }
    formatted.distinct
  }


  /**
    * main
    *
    * @param args
    */
  def main(args: Array[String]) {
    var keyword_dir: File = null
    var date: String = null
    for (i <- 0 until args.length) {
      val a = args(i)
      if (a == "-k" || a == "--keyword_dir") {
        keyword_dir = new File(args(i + 1))
      } else if (a == "-d" || a == "--date") {
        date = args(i + 1)
      }
    }

    val conf = new SparkConf().setAppName(s"QueryKeywordSearchClassify")
    val sc = new SparkContext(conf)

    assert(keyword_dir != null)
    // load
    val kws = load_keywords(keyword_dir)
    val classifier = new KeywordSearchClassifier(kws)

    val queries = getQueryByDate(sc, date)
    queries.cache

    queries.take(10).foreach(println)

    val categorized = categorize(classifier, queries)
    val formatted = format4dsp(date, categorized).distinct

    // debug
    formatted.take(100).foreach(println)
    val date2 = date.replace("-", "")
    //val opath = s"tachyon://192.168.190.200:19998/data/dsp/segments/$date/d2c-$date2-180000-.json"
    //val opath = s"hdfs://192.168.190.200:9000/data/dsp/segments/search/$date/d2c-$date2-180000-.tsv"
    //val opath = s"/user/r2a/data/dsp/segments/search/$date/d2c-$date2-180000-.tsv"
    val opath = s"hdfs://192.168.190.200:9000/data/dsp/segments/search/$date/d2c-$date2-180000-.json"
    formatted.saveAsTextFile(opath)
  }
}

