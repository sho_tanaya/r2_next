package com.d2c.be.r2_next.nlp

import scala.collection.mutable.ArrayBuffer

/**
 * Created by mrikitoku on 15/09/16.
 */
class Token(morphemeStr: String) extends MorphAcceptForKeyword with Serializable {
  val morphs        = morphemeStr.split(Token.DELIMITER).map(m => Morph(m)).filter(_ != null)
  val morphNgrams   = ArrayBuffer.empty[Seq[Morph]]
  val surfaceNgrams = ArrayBuffer.empty[String]

  override def toString : String = {
    "Token(" + this.morphs.map(_.toString).mkString(Token.DELIMITER) + ")"
  }

  def setSequentialNgramExpand(max: Int = 4) : Token = {
    val len = this.morphs.size
    val ngrams = ArrayBuffer.empty[String]
    for (i <- 0 until len) {
      for (n <- 0 to max) {
        val end = i + n
        if (end <= len) {
          val ngram = this.morphs.slice(i, end).mkString(Token.DELIMITER)
          if (accept(ngram)) {
            ngrams += ngram
            val ng = this.morphs.slice(i, end)
            this.morphNgrams += ng
            this.surfaceNgrams += ng.map(m => m.surface).mkString("_")
          }
        }
      }
    }
    this
  }


  def ngramCrossJoin(otherToken: Token) : Seq[String] = {
    if (otherToken.surfaceNgrams.size == 0) {
      otherToken.setSequentialNgramExpand()
    }
    this.ngramCrossJoin(otherToken.surfaceNgrams)
  }


  def ngramCrossJoin(otherNgrams: Seq[String]) : Seq[String] = {
    val cj = ArrayBuffer.empty[String]
    if (this.surfaceNgrams.size == 0) {
      setSequentialNgramExpand()
    }
    this.surfaceNgrams.foreach {
      case ng => {
        otherNgrams.foreach {
          case ong => {
            val nng = Array(ng, ong).mkString(Token.DELIMITER)
            cj += nng
          }
        }
      }
    }
    cj
  }
}


object Token {
  val DELIMITER = " "
  def apply(morphemeStr: String) : Token =  {
    new Token(morphemeStr)
  }
}