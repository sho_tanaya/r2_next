package com.d2c.be.r2_next.nlp

/**
 * Created by mrikitoku on 15/08/26.
 */

object NgramFilter {

  def accept(ngram: Seq[String]): Boolean = {
    val len = ngram.size
    val morphems = ngram.mkString(" ")
    if (len == 1) {
      val m = ngram(0)
      if (m.indexOf("接尾") >= 0) {
        return false
      }
      if (m.indexOf("接頭") >= 0) {
        return false
      }
      if (m.indexOf(",数,") >= 0) {
        return false
      }
      if (m.indexOf("記号") >= 0) {
        return false
      }
    }

    val start = ngram(0)
    if (start.indexOf("接尾") >= 0) {
      return false
    }
    if (start.indexOf("記号") >= 0) {
      return false
    }

    // 末尾
    if (len > 1) {
      val end = ngram(len - 1)
      if (end.indexOf("接頭") >= 0) {
        return false
      }
      if (end.indexOf("記号") >= 0) {
        return false
      }
    }
    // 数字のみ
    val text = ngram.map(t => t.split(",")(0)).mkString("")
    !text.matches("\\d+")
  }
}