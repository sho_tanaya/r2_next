package com.d2c.be.r2_next.nlp

/**
 * Created by mrikitoku on 15/07/28.
 */
abstract class Accept {
  def accept(feature: String) : Boolean = false
}
