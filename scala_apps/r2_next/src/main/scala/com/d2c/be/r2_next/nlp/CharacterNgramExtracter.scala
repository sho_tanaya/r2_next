package com.d2c.be.r2_next.nlp

import java.util

import com.rikima.ml.{Hashing, Feature}

import com.d2c.be.r2_next.nlp.CharacterTable.CharacterType

import scala.collection.mutable._

/**
 * Created by mrikitoku on 15/08/28.
 */
object CharacterNgramExtracter {

  def extract(text: String) : Seq[Feature] = {
    val normalizedText = CharacterSplitter.splitedText(text)
    val tmp = new util.TreeMap[Long, Double]
    val chunks = normalizedText.split(" ")
    // debug
    println("normalize text:" + normalizedText)
    for (chunk <- chunks) {
      var ngram = 3
      val len = chunk.length
      val t = CharacterTable.getCharacterType(chunk.charAt(0))
      for (i <- 0 until len) {
        if (t == CharacterType.KANJI) {
          ngram = 2
        }
        val start = i
        val end = if ((i + ngram) <= len) {
          i + ngram
        } else {
          len
        }
        val unit = chunk.substring(start, end)
        if ((i > 0 & unit.length < ngram) || unit.length == 1 ) {
        } else {
          // debug
          println(s"$ngram-gram:" + unit)
          val f = Hashing.hash(unit, 1, 1 << 20)
          //Hashing.feautureId(unit)

          if (!tmp.containsKey(f)) {
            tmp.put(f, 0)
          }
          val v = tmp.get(f)
          tmp.put(f, v + 1)
        }
      }
    }

    val fs = ArrayBuffer.empty[Feature]
    val iter = tmp.entrySet().iterator()
    while (iter.hasNext) {
      val e = iter.next
      val k = e.getKey
      val v = e.getValue

      val feature = new Feature(k.asInstanceOf[Int], v)
      fs += feature
    }
    fs
  }
}
