package com.d2c.be.r2_next.nlp

import scala.collection.mutable.ArrayBuffer

/**
 * Created by mrikitoku on 15/09/16.
 */
class Query(query: String) extends Serializable {
  var tokens: Seq[Token] = null

  def getQueryString : String = this.query

  def parse(tokenizer: R2Tokenizer): Query = {
    //val q = this.query.replace(" ",Query.DELIMITER)
    this.tokens = tokenizer.parse(this.query).mkString(" ").split(Query.DELIMITER).map(_.trim)
      .map(Token(_))
    this
  }

  def getNgrams() : Seq[String] = {
    val ngrams = ArrayBuffer.empty[String]
    this.tokens.map( t => t.setSequentialNgramExpand())
    // uniqgram
    this.tokens.foreach {
      case t => ngrams ++= t.surfaceNgrams
    }
    // bigram
    val len = this.tokens.length
    for (i <- 0 until len) {
      val t_i = this.tokens(i)
      for (j <- i+1 until len) {
        val t_j = this.tokens(j)
        ngrams ++= t_i.ngramCrossJoin(t_j)
      }
    }
    ngrams
  }

  override def toString : String = {
    "Query(" + this.tokens.map(_.toString).mkString("\n") + ")"
  }
}

object Query {
  val DELIMITER = "　"

  def apply(query: String) : Query = {
    new Query(query)
  }
}
