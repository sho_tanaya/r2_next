package com.d2c.be.r2_next.adn

import java.util.Calendar

import org.apache.spark.SparkContext
import org.apache.spark.internal.Logging
import org.apache.spark.rdd.RDD
import org.joda.time.DateTime

/**
  * Created by mrikitoku on 15/07/29.
  */

case class DadnQueryUnit(keyword: String, user_id: Long, date: Int)

case class DadnQueryRow(keyword: String, user_id: String, dateStr: String) {
  def intDate: Int = {
    this.dateStr.split("-").mkString("").toInt
  }
}

class DadnQuery(path_prefix: String = "hdfs://r2a001:9000/data/dadn/query/") extends Serializable with Logging {
  val DELIMITER = "\t"

  val KEYWORD_INDEX = 0
  val USER_ID_INDEX = 1
  val DATE_INDEX = 2
  val F81_ID_INDEX = 3
  val APP_ID_INDEX = 4
  val IDFA_INDEX = 5

  def asRdd(sc: SparkContext, date: String): RDD[Seq[String]] = {
    val path = s"${path_prefix}/${date}/query2.tsv"
    val rdd = sc.textFile(path).map(l => l.split(DELIMITER).toSeq)
    //logInfo("#rdd " +  rdd.count)
    rdd
  }


  def asRdd(sc: SparkContext, end: String, daysAgo: Int = 0): RDD[String] = {
    val path = s"${path_prefix}/${end}/query2.tsv"
    var rdd = sc.textFile(path)

    val ss = end.split("-")
    val end_y = ss(0).toInt
    val end_m = ss(1).toInt
    val end_d = ss(2).toInt
    val date = Calendar.getInstance
    date.set(end_y, end_m - 1, end_d)
    for (i <- 1 to daysAgo) {
      date.add(Calendar.DAY_OF_MONTH, -1)
      val format = "%tF" format date
      val path = s"${path_prefix}/${format}/query2.tsv"
      val d = sc.textFile(path)
      rdd = rdd.union(d)
    }
    rdd
  }

  def asDadnQueryRow(sc: SparkContext, date: String): RDD[DadnQueryRow] = {
    asRdd(sc, date).map(ss => DadnQueryRow(ss(KEYWORD_INDEX), ss(USER_ID_INDEX), ss(DATE_INDEX)))
  }

  def asDadnQueryUnit(sc: SparkContext, end: String, days: Int = 30): RDD[DadnQueryUnit] = {
    val queries = asDadnQueryRow(sc, end, days)
    val uniqueUsers = monthlyUU(sc, end, days)

    queries.map[(String, DadnQueryRow)](row => (row.user_id, row)).join(uniqueUsers.map(x => (x._1._1, x._2))).map[(DadnQueryRow, Long)](t => t._2).
      zipWithIndex.map(t => DadnQueryUnit(t._1._1.keyword, t._1._2, t._1._1.intDate))
  }


  def monthlyUU(sc: SparkContext, date: String, days: Int = 30): RDD[((String, Option[String], Option[String], Option[String]), Long)] = {
    val queries = asRdd(sc, date, days)
    val users = queries.map(l => {
      var splited = l.split(DELIMITER).map(x => x.trim)
      val segId: String = splited.lift(USER_ID_INDEX).getOrElse("")
      val f81Id: Option[String] = splited.lift(F81_ID_INDEX)
      val appId: Option[String] = splited.lift(APP_ID_INDEX)
      val idfa: Option[String] = splited.lift(IDFA_INDEX)


      /*
      val (f81Id, appId): (Option[String], Option[String]) = {
        if (splited.length > APP_ID_INDEX) {
          (Some(splited(F81_ID_INDEX)), Some(splited(APP_ID_INDEX)))
        }
        else if (splited.length > F81_ID_INDEX) {
          if (splited(F81_ID_INDEX).contains("-")) {
            (None, Some(splited(F81_ID_INDEX)))
          }
          else {
            (Some(splited(F81_ID_INDEX)), None)
          }
        }
        else {
          (None, None)
        }
      }
      */

      (segId, f81Id, appId, idfa)
    }).distinct
    users.zipWithIndex.map(t => (t._1, t._2 + 1)).cache
  }


  def asDadnQueryRow(sc: SparkContext, date: String, days: Int = 0): RDD[DadnQueryRow] = {
    asRdd(sc, date, days).map(l => l.split(DELIMITER, -1)).map(ss => DadnQueryRow(ss(KEYWORD_INDEX), ss(USER_ID_INDEX), ss(DATE_INDEX)))
  }

  def asDadnQueryUnit(sc: SparkContext, queries: RDD[DadnQueryRow], uniqueUsers: RDD[(String, Long)]): RDD[DadnQueryUnit] = {
    queries.map[(String, DadnQueryRow)](row => (row.user_id, row)).join(uniqueUsers).map[(DadnQueryRow, Long)](t => t._2).
      zipWithIndex.map(t => DadnQueryUnit(t._1._1.keyword, t._1._2, t._1._1.intDate))
  }
}
