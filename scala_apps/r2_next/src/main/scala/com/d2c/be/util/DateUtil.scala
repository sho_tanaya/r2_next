package com.d2c.bicc.util

import java.util.Calendar

/**
 * Created by mrikitoku on 15/07/29.
 */
object DateUtil {
  def agoDate(date: String, ago: Int): String = {
    val cal = Calendar.getInstance

    val ss = date.split("-")
    val end_y = ss(0).toInt
    val end_m = ss(1).toInt
    val end_d = ss(2).toInt
    cal.set(end_y, end_m - 1, end_d)

    cal.add(Calendar.DAY_OF_MONTH, -ago)
    val format = "%tF" format cal
    format
  }
}