package com.d2c.bicc.r2a.dsp.classifier


import com.d2c.be.r2_next.dsp.{Category, DspSegmentUtil, Query}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.internal.Logging


/**
  * Created by masakir on 2015/11/15.
  */

object QueryClassify extends DspSegmentUtil with Logging {
  /**
   * categorize
   *
   * @param classifier
   * @param queries
   * @return
   */
  def categorize(classifier: QueryClassifier, queries: RDD[Query]): RDD[(String, Category, Int, String, Double)] = {
    logInfo("categorize by classifier")
    val bcClassifier = queries.context.broadcast(classifier)
    val categorized = queries.flatMap {
      case q => {
        val query   = q.text
        val userId = q.userId
        val predict = bcClassifier.value.categorize(query)
        predict.filter {
          case (c, y, s) => y > 0 && c.id > 0
        }.map {
          case (category, y, score) => {
            (userId, category, y, query, score)
          }
        }
      }
    }.filter {
      case (userId, category, y, text, score) => (y > 0)
    }.filter {
      case (userId, category, y, text, score) => category.id > 0
    }.filter {
      case (userId, category, y, text, score) => userId != "NULL"
    }
    categorized
  }


  def format4dsp(date: String, categorized: RDD[(String, Category, Int, String, Double)]): RDD[String] = {
    val formatted = categorized.map {
      case (userId, category, y, query, score) => {
        val cid = category.id
        Array(date, s"""{"aid": "d2c", "sid": "DCMsearchcategory", "uid": "$userId", "category$cid": 1 }""", query, category.id, score).mkString("\t")
      }
    }
    formatted.distinct
  }

  /**
   * main
    *
    * @param args
   */
  def main(args:Array[String]) {
    var modelDir: String = null
    var date: String = null
    var num_partitions = 100
    var input = ""
    for (i <- 0 until args.length) {
      val a = args(i)
      if (a == "-m" || a == "--model_dir") {
        modelDir = args(i+1)
      } else if (a == "-d" || a == "--date") {
        date = args(i+1)
      } else if (a == "-p" || a == "--num_partitions") {
        num_partitions = args(i+1).toInt
      } else if (a == "-i" || a == "--input_path") {
        input = args(i+1)
      }
    }

    val conf = new SparkConf().setAppName(s"QueryClassify")
    val sc = new SparkContext(conf)

    if (modelDir == null) {
      println("please input model dir")
      System.exit(1)
    }
    val classifier = new QueryClassifier
    classifier.loadFromLocalDir(modelDir)

    // TODO
    val queries = getQuery(sc, input)
    queries.repartition(100)
    queries.cache
    queries.take(10).foreach(println)

    val categorized = categorize(classifier, queries).repartition(100)
    val formatted   = format4dsp(date, categorized)

    // debug
    formatted.take(100).foreach(println)

    val date2 = date.replace("-", "")
    //val opath = s"tachyon://192.168.190.200:19998/data/dsp/segments/$date/d2c-$date2-180000-.json"
    val opath = s"hdfs://192.168.190.200:9000/data/dsp/segments/classifier/$date/d2c-$date2-180000-.json"
    formatted.saveAsTextFile(opath)
  }
}
