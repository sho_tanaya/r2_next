package com.d2c.be.r2_next.nlp

/**
  * Created by mrikitoku on 15/12/28.
  */
trait CharacterNormalize {

  def normalize(text: String, replaceSpace: Boolean = false): String = {
    CharacterNormalizer.normalize(text, replaceSpace)
  }
}
