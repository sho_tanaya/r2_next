package services

/**
 * Created by mrikitoku on 15/09/24.
 */
trait SegmentLevel {
  def setLevel(uu: Int) : Int = {
    if (uu < 100) {
      0
    } else if (uu < 500) {
      1
    } else if (uu < 3000) {
      2
    } else if (uu < 5500) {
      3
    } else if (uu < 8000) {
      4
    } else {
      5
    }
  }
}