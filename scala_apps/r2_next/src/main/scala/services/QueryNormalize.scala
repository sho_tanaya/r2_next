package services

/**
 * Created by mrikitoku on 15/09/25.
 */
trait QueryNormalize {
  def normalizeQuery(query: String) : String = {
    if (query != null) {
      query.replace("　", " ").replace("｜", "|")
    } else {
      ""
    }
  }
}