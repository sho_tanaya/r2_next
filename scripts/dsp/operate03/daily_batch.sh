#!/bin/sh
cur=$(dirname $0)
. $cur/logger.sh

host=172.17.19.110
port=9010

export JAVA_HOME=/usr/java/1.7

date=$(date -d '2 day ago' +'%Y-%m-%d')
if [ ! -z $1 ] ; then
    date=$1
fi
echo $date

# export query
start_log "export_dsp_query $date" $cur/daily_batch.log
$cur/export_dsp_query.sh $date
end_log "export_dsp_query $date" $cur/daily_batch.log

# hadoop import
start_log "hadoop import $date" $cur/daily_batch.log
$cur/hadoop_load_dsp_query.sh $date
end_log "hadoop import $date" $cur/daily_batch.log

#
# check result file
#

prefix=dsp_query

null_check=$(head $cur/data/${prefix}_${date}.tsv | awk -F'\t' '{print $2}' | sort | uniq)

if [ -s $cur/data/${prefix}_${date}.tsv ] && [ ! "${null_check}" = "NULL" ]; then
    log "[END] export dsp_query log" $cur/daily_batch.log
    content=$(tail -n 10 $cur/data/${prefix}_${date}.tsv|nkf -j)
    $cur/send_mail.sh "[operate03][search_category_targeting] succeeded to export and upload the dsp_query_$date.tsv" "$content"
    rm -rf $cur/data/*
    exit 0
else
    content=$(head $cur/data/${prefix}_${date}.tsv)
    $cur/send_mail.sh "[ERROR][operate03][search_category_targeting] failed to export and upload the dsp_query_$date.tsv" "$content"
    log "[ERROR] ${table}_${date}.tsv is invalid" $cur/daily_batch.log

    exit 1         
fi

