set ver="${hiveconf:ver}";
set key="d2cbiccr2aaurum2";

insert overwrite directory '/user/r2a/data/'
select
  keyword,
  user_id,
  ver
from platinum.pt_dmenu_query
where 
 ver = ${hiveconf:ver}
 and length(trim(user_id)) > 0
 and keyword not rlike "fin000000|biz000000|shl000000|lif000000|ent000000|bty000000|d20000|じぶんローン|オリックス銀行|スマートニュース|ひし餅"
