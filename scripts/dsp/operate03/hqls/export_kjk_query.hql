add jar hdfs://nameservice1/system/extjars/udf/util_udf-assembly-0.0.1-SNAPSHOT.jar;

create temporary function cipher as 'com.d2c.bicc.hive.udf.CipherUdf';
create temporary function dow as 'com.d2c.bicc.hive.udf.DayOfWeekUdf';
create temporary function woy as 'com.d2c.bicc.hive.udf.WeekOfYearUdf';
create temporary function ctm as 'com.d2c.bicc.hive.udf.CurrentTimemillisUdf';

set ver="${hiveconf:ver}";

set key="d2cbiccr2aaurum2";

select 
 log_date,
 log_time,
 trn_id,
 keyword,
 page,
 pid,
 given_lct,
 actual_lct,
 menu_id,
 pkind,
 donotsell_kw_flg,
 cipher(suid, ${hiveconf:key}, "encryption") e_suid,
 genko_type,
 ver
from kjk_query 
where 
 ver = ${hiveconf:ver}
 and trn_id not rlike "^xxxx" 
;
