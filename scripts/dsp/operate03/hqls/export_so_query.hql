add jar hdfs://nameservice1/system/extjars/udf/util_udf-assembly-0.0.1-SNAPSHOT.jar;

create temporary function cipher as 'com.d2c.bicc.hive.udf.CipherUdf';
create temporary function dow as 'com.d2c.bicc.hive.udf.DayOfWeekUdf';
create temporary function woy as 'com.d2c.bicc.hive.udf.WeekOfYearUdf';
create temporary function ctm as 'com.d2c.bicc.hive.udf.CurrentTimemillisUdf';

set ver="${hiveconf:ver}";
set key="d2cbiccr2aaurum2";

select 
 t.keyword,
 t2.dspid,
 t.ver,
 t.dcmid
from
(
select 
 keyword,
 user_id dcmid,
 ver
from query_for_dsp.dsp_query
where 
 ver = ${hiveconf:ver}
 and length(trim(user_id)) > 0
 and keyword not rlike "fin000000|biz000000|shl000000|lif000000|ent000000|bty000000|d20000|じぶんローン|オリックス銀行|スマートニュース|ひし餅"
group by
 keyword,
 user_id,
 ver
) t
join (
select
 dcmid,
 dspid
from query_for_dsp.id_table
where
 ver = ${hiveconf:ver}
<<<<<<< HEAD
 -- ver = "2015-11-23"
=======
>>>>>>> a44b8f9fee9ea4c86f7e4df8a43029209a196581
) t2
on
 t.dcmid = t2.dcmid
;
