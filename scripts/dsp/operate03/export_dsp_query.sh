#!/bin/sh
home=/home/mrikitoku
cur=$(dirname $0)
. $cur/logger.sh
addresses="masaki.rikitoku@d2c.co.jp,atsushi.murakami@d2c.co.jp,kazuyoshi.takayama@d2c.co.jp,kentaro.suzuki@d2c.co.jp"

table=dsp_query

date=$(date -d '2 day ago' +'%Y-%m-%d')
if [ ! -z $1 ] ; then
    date=$1
fi
echo $date

log "[START] export dsp_query log" $cur/daily_batch.log
hive --hiveconf ver="$date" -f $cur/hqls/export_${table}.hql > $cur/data/${table}_${date}.tsv
if [ -s $cur/data/${table}_${date}.tsv ] ; then
    log "[END] export dsp_query log" $cur/daily_batch.log
    content=$(tail -n 10 $cur/data/${table}_${date}.tsv|nkf -j)
    echo $content
    $cur/mail.py "[operate03] success to export and upload the dsp_query_$date.tsv" "$content" $addresses 
else
    log "[ERROR] ${table}_${date}.tsv is invalid" $cur/daily_batch.log
fi

