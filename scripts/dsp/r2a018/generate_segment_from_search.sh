#!/bin/sh
cur=$(dirname $0)
r2a_home=/opt/r2a/
hadoop_prefix=/opt/hadoop/

host=172.17.19.110
port=9010

date=$1
days_ago=$2
if [ -z ${date} ] || [ -z ${days_ago} ]; then
    echo "please input days ago to be set"
    exit 1
fi 

date2=$(date -d $date +'%Y%m%d')
echo $date $date2

target_date=$(date -d "${date} ${days_ago} days ago" +'%Y%m%d')
echo $target_date

/opt/hadoop/bin/hdfs dfs -getmerge "/data/dsp/segments/search/${date}/d2c-${date2}-180000-.json" ${cur}/out.txt

if [ ! -s $cur/out.txt ] ; then
    echo "no hdfs result"
    exit 1
fi

cat ${cur}/out.txt | awk -F'\t' '{print $2}' > $cur/segments/d2c-${date2}-180000-.json

categories=$r2a_home/data/train_data/category_ids.txt
while read line ; do
    # echo ${line}
    cid=$(echo $line | awk '{print $2}')
    name=$(echo $line | awk '{print $3}')
    
    cat $cur/out.txt | grep -w category$cid > $r2a_home/data/train_data/$cid/results.txt
    
    # post results.txt
    results=$r2a_home/data/train_data/$cid/results.txt
    curl -POST http://$host:$port/r2a/dsp/keywords/$cid/results.txt/manage_edit/?title=results.txt -F data=@$results \
        --user dsp:dsp \
     	-v 1>/dev/null 2>/dev/null  

    results=$r2a_home/data/train_data/$cid/results.txt
    # to csv
    cat $results | awk -F'\t' '{print $1","$2","$4}' | iconv -f UTF8 -t SJIS -c > $r2a_home/data/train_data/$cid/results.csv
    csv=$r2a_home/data/train_data/$cid/results.csv
    curl -POST http://$host:$port/r2a/dsp/keywords/$cid/results.csv/manage_edit/?title=results.csv -F data=@$csv \
        --user dsp:dsp \
    	-v 1>/dev/null 2>/dev/null 

   # count
   curl -GET http://$host:$port/r2a/dsp/keywords/$cid/count.csv --user dsp:dsp > $cur/count.tmp
   echo "${date},`cat $r2a_home/data/train_data/$cid/results.csv | wc -l`" >> $cur/count.tmp
   count=$cur/count.tmp
   curl -POST http://$host:$port/r2a/dsp/keywords/$cid/count.csv/manage_edit/?title=count.csv -F data=@$count \
        --user dsp:dsp \
        -v 1>/dev/null 2>/dev/null
   rm $cur/count.tmp
done < $categories
rm -rf $cur/out.txt
