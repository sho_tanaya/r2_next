#!/bin/sh
cur=$(dirname $0)

host=172.17.19.110
port=9010


for id in `curl -GET http://$host:$port/r2a/dsp/keywords/category_ids --user d2cadmin:d2cadmin --silent | awk '{print $1}'`
do
  mkdir -p ${cur}/${id}

  curl -GET http://$host:$port/r2a/dsp/keywords/${id}/keywords.txt \
  --user d2cadmin:d2cadmin \
  -o ${cur}/${id}/keywords.txt
done


