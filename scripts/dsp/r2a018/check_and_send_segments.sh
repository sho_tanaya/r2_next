#!/bin/bash
cur=$(dirname $0)

export HTTPS_PROXY=http://192.168.189.100:3128
export HTTP_PROXY=http://192.168.189.100:3128

host=172.17.19.110
port=9010

export AWS_ACCESS_KEY_ID=AKIAJRT6EQLDO5PVMYOA
export AWS_SECRET_ACCESS_KEY=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
export bucket=seg-prod-data

date=$1
days_ago=$2

echo ${date} ${days_ago}

if [ -z "${date}" ] || [ -z ${days_ago} ]; then
    echo "wrong input"
    exit 1
fi

date2=$(date -d $date +'%Y%m%d')
target_date2=$(date -d "$date $days_ago days ago" +'%Y%m%d')

echo $target_date2

##
# check and send
#
#
filename=`ls $cur/segments/d2c-${date2}*.json | tail -1`
echo "json file name ${filename}"
if [ -f "${filename}" ] ; then
    curl -POST http://$host:$port/r2a/dsp/segments/segment/manage_edit/?title=$filename -F data=@$filename -v --user dsp:dsp >/dev/null 2>&1

    aws s3 cp "${filename}" "s3://${bucket}/production_data/d2c/segment_dsp/search/DCMkensakucategory.json"
    aws s3 ls "s3://${bucket}/production_data/d2c/segment_dsp/search/DCMkensakucategory.json"

    curl -GET http://$host:$port/r2a/dsp/segments/count.csv --user dsp:dsp > $cur/count.tmp
    echo "${date},`cat ${filename} | wc -l`" >> $cur/count.tmp
    count=$cur/count.tmp
    curl -POST http://$host:$port/r2a/dsp/segments/count.csv/manage_edit/?title=count.csv -F data=@$count \
        --user dsp:dsp \
        -v 1>/dev/null 2>/dev/null
    rm $cur/count.tmp


    #$cur/aws/sync_s3_dir.sh
    $cur/utils/send_mail.sh "[r2a018][search_category_targeting] succeeded to generate segments and send" "OK"

    #prev=$(date -d "$date 1 day ago" +'%Y-%m-%d')
    #hdfs dfs -rm -r -f /data/dsp/query/$prev

    exit 0
else
    $cur/utils/send_mail.sh "[ERROR][r2a018][search_category_targeting] failed to generate segments" "NG"

    exit 1
fi
