#!/bin/sh

function log() {
    message=$1
    logfile=$2

    dt=$(date +'%Y-%m-%d %H:%M:%S')
    echo "[$dt] : $message" >>$logfile
}


function start_log() {
    message=$1
    logfile=$2

    dt=$(date +'%Y-%m-%d %H:%M:%S')
    echo "[$dt] : [START] $message" >>$logfile
}

function end_log() {
    message=$1
    logfile=$2

    dt=$(date +'%Y-%m-%d %H:%M:%S')
    echo "[$dt] : [END] $message" >>$logfile
}