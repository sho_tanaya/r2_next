#!/usr/bin/env python
#!-*- coding: utf-8 -*-

import sys
import smtplib
from email.mime.text import MIMEText

me = "masaki.rikitoku@d2c.co.jp,kentaro.suzuki@d2c.co.jp,kazuyoshi.takayama@d2c.co.jp,atsushi.murakami@d2c.co.jp"

def send(title, body, address_list, host):
    msg = MIMEText(body)
    # me == the sender's email address
    # you == the recipient's email address
    msg['Subject'] = title
    msg['From'] = me
    s = smtplib.SMTP(host)
    s.sendmail(me, address_list.split(','), msg.as_string())
    s.quit()


if __name__ == '__main__':
    title = sys.argv[1]
    body = sys.argv[2]
    address_list = sys.argv[3]
    host = '192.168.189.100'
    print body
    send(title, body, address_list, host)
