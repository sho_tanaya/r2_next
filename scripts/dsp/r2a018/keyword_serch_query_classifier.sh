#!/bin/sh
cur=$(dirname $0)
export JAVE_HOME=/usr/java/1.7
export hadoop_prefix=/opt/hadoop
export r2a_home=/opt/r2a
export spark_home=/opt/spark

date=$1
if [ -z ${date} ] ; then
    echo "wrong input"
    exit 1
fi

jar=$r2a_home/target/scala-2.10/r2a_2.10-1.0.jar
jars=$r2a_home/target/scala-2.10/r2a_2.10-1.0.jar
for j in $(ls $r2a_home/lib/*.jar) ; do
    jars=$jars:$j
done

program=com.d2c.bicc.r2a.dsp.search.QueryKeywordSearchClassify

kw_dir=$r2a_home/data/train_data

# rm segments
$hadoop_prefix/bin/hdfs dfs -rm -r -f /data/dsp/segments/search/$date/

# generate segments
$spark_home/bin/spark-submit --num-executors 18 --executor-cores 2 --class $program --master yarn  --deploy-mode client --jars $jars $jar -d $date -k $kw_dir

exit $?
