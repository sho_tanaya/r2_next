#!/bin/sh
cur=$(dirname $0)

. $cur/../batch_config.txt

# get f81 users
$cur/get_f81_users.sh $yesterday

# get filtered_keywords
$cur/get_filtered_frequent_keywords.sh $yesterday

# send result mail
f81_users=$(ls $cur/f81_users_${yesterday}/)
filtered_kws=$(ls $cur/filtered_frequent_keywords_${yesterday}/)
if [ -s $cur/f81_users_${yesterday}/users.txt ] && [ -s $cur/filtered_frequent_keywords_${yesterday}/source.txt ] ; then
    echo "send mail"
    $cur/../send_mail.sh "[r2a018] import f81 users and filtered keywords $date" "$date ${f81_users} ${filtered_kws}"
    exit 0
fi
exit 1
