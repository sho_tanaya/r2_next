#!/bin/sh
cur=$(dirname $0)

date=$1
if [ -z $date ] ; then
    date=$(date -d '1 day ago' +'%Y-%m-%d')  
fi
yesterday=$(date -d "$date 1 day ago" +'%Y-%m-%d')  

if [ -d $cur/f81_users_$yesterday ] ; then
    rm -rf $cur/f81_users_$yesterday
fi

if [ ! -d $cur/f81_users_$date ] ; then
    mkdir $cur/f81_users_$date
fi

if [ ! -z $cur/f81_users_$date/users.txt ] ; then
    rm -rf $cur/f81_users_$date/users.txt
fi

/opt/hadoop/bin/hdfs dfs -getmerge /data/dadn/frequent_keywords/${date}/mau2 $cur/f81_users_${date}/users.txt
