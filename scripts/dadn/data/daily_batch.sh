#!/bin/sh
cur=$(dirname $0)
. $cur/logger.sh

date=$1
if [ -z $date ] ; then
    date=$(date +'%Y-%m-%d')  
fi


# get_optout user
log "[START] get_optout_user" $cur/daily_batch.log
$cur/get_optout_user.sh
log "[END] get_optout_user" $cur/daily_batch.log

# get request
log "[START] get_segments_request" $cur/daily_batch.log
$cur/get_segments_request.sh
log "[END] get_segments_request" $cur/daily_batch.log

# check optout request
d=$(date -d "$date" +%Y%m%d)
optout=$(ls $cur/optout/$date/docomo_optout_v1_$d.tsv)
echo $optout

# request 
request=$(ls $cur/search_seggments_request/$date/docomo_search_segments_request_v1_$d.json)
echo $request

addresses=masaki.rikitoku@d2c.co.jp,rikima3132@gmail.com
$cur/../mail.py "[jyudan-dbmg] updated optout and request data" "$optout $request" "$addresses"



