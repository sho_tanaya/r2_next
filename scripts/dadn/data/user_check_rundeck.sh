#!/bin/sh
cur=$(dirname $0)

. $cur/../batch_config.txt

# check user_test
$cur/user_check.sh $date >$cur/../test_users
num_users=$(cat $cur/../test_users | wc -l)
echo $num_users
if [ $num_users -ge 2 ] ; then
    exit 0
else
    exit 1
fi
