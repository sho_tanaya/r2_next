#!/usr/bin/env python
#! -*- coding: utf-8 -*-
import sys

f81id="1cae4fbc32f7420f9cd83e94fb58b47e"

def check(fname):
    io = open(fname)
    for i, l in enumerate(io):
        ss = l.split("\t")
	if len(ss[0]) != len(f81id):
		print ss[0]
	assert len(ss[0]) == len(f81id)


    print i
    assert i > 0

if __name__ == '__main__':
    try:
        check(sys.argv[1])
    except:
        sys.stderr.write("[ERROR] segments_checker.py : check segments is false\n" + str(sys.argv))
        sys.exit(1)
