#!/bin/sh
cur=$(dirname $0)
target=$1
if [ -z $target ] ; then
    echo "please input target dir"
    exit
fi

pushd $target
for d in $(ls . | grep -v txt) ; do
    echo $d
    if [ ! -z $d.txt ] ; then
	rm -rf $d.txt
    fi
    touch $d.txt
    
    cat $d/part* >> $d.txt
done
popd