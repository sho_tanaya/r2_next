#!/bin/sh
cur=$(dirname $0)

date=$1
if [ -z $date ] ; then
    date=$(date -d '1 day ago' +'%Y-%m-%d')  
fi
yesterday=$(date -d "$date 1 day ago" +'%Y-%m-%d')


if [ -d $cur/filtered_frequent_keywords_$yesterday ] ; then
    rm -rf $cur/filtered_frequent_keywords_$yesterday
fi

if [ ! -d $cur/filtered_frequent_keywords_$date ] ; then
    mkdir $cur/filtered_frequent_keywords_$date
fi

source=$cur/filtered_frequent_keywords_$date/source.txt

/opt/hadoop/bin/hdfs dfs -getmerge /data/dadn/frequent_keywords/${date}/frequent_keywords2.tsv $cur/fks.txt
cat $cur/fks.txt | awk -F'\t' '{print $1"\t"$3}' > $source

