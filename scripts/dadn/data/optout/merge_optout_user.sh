#!/bin/sh
cur=$(dirname $0)
date=$(date +'%Y-%m-%d')
if [ ! -z $1 ] ; then                                                                                                
    date=$1                                                                                                          
fi
date2=$(date -d $date +'%Y%m%d')

mkdir -p ${cur}/data/${date}
cat ${cur}/docomo_tmp ${cur}/f81_tmp > ${cur}/data/${date}/docomo_optout_v1_${date2}.tsv

rm ${cur}/docomo_tmp
rm ${cur}/f81_tmp

exit $?
