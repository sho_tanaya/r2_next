#!/usr/bin/env python
#! -*- coding: utf-8 -*-

import sys

f81id="1cae4fbc32f7420f9cd83e94fb58b47e"


def check(fname):
    io = open(fname)
    for i, l in enumerate(io):
        ss = l.split("\t")
        assert len(ss) == 3
        
        seg_id = int(ss[0])
        uu     = int(ss[1])
        level  = int(ss[2])
        assert level >= 0
        assert level <= 5
        if uu < 100:
            assert level == 0
    
    assert i > 0

if __name__ == '__main__':
    try:
        check(sys.argv[1])
    except:
        sys.stderr.write("[ERROR] segments_uu_checker.py : check uu is false\n" + str(sys.argv))
        sys.exit(1)
