#!/usr/bin/env python
#! -*- coding: utf-8 -*-

import re
import sys

pat=re.compile(r"List\((.+)\)")
def execute(fname):
    io = open(fname)
    for l in io:
        l = l.strip()
        if l.startswith("(") and l.endswith(")"):
            l = l[1:-1]
        
        ss = l.split(",")[:2]
        m = pat.search(l)
        
        if m:
            ss.append(m.group(1))

        print "\t".join(ss)

if __name__ == '__main__':
    execute(sys.argv[1])
