#!/usr/bin/env python
#! -*- coding: utf-8 -*-
import sys

appid="362cdb10-6baa-4903-b773-caed8021c1e3"

def check(fname):
    io = open(fname)
    for i, l in enumerate(io):
        ss = l.split("\t")
	if len(ss[0]) != len(appid):
		print ss[0]
	assert len(ss[0]) == len(appid)


    print i
    assert i > 0

if __name__ == '__main__':
    try:
        check(sys.argv[1])
    except:
        sys.stderr.write("[ERROR] segments_checker.py : check segments is false\n" + str(sys.argv))
        sys.exit(1)
