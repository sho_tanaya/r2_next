#!/bin/sh
cur=$(dirname $0)
. $cur/logger.sh

date=$(date -d '1 day ago' +'%Y-%m-%d') 
if [ ! -z $1 ] ; then                       
    date=$1                                                                    
fi                               
shift         
echo $date      

source=$cur/data/filtered_frequent_keywords_$date/source.txt

if [ -s $source ] && [ -f $source ] ; then
    log "[START] indexing" $cur/daily_batch.log 
    $cur/indexing.sh -i $source --all_delete -index $cur/index_new
    if [ "$?" = "0" ] ; then
	log "[END] indexing" $cur/daily_batch.log
    else
	log "[ERROR] failed at indexing" $cur/daily_batch.log
    fi
    # check
    if [ ! -z $cur/index_bk ] ; then
	rm -rf $cur/index_bk
    fi
    mv $cur/index $cur/index_bk
    mv $cur/index_new $cur/index

    # これは、ローカルの検索サーバーの更新確認
    # コメントアウト
    #$cur/update.sh > $cur/update.log
    status=$(cat $cur/update.log)
    if [ "${status}" = "OK" ] ; then
	echo $
	log "[INFO] update: $status " $cur/daily_batch.log 
	# update indices
	$cur/update_index.sh
    else
	log "[ERROR] failed to update index" $cur/daily_batch.log 
    fi
else 
    log "[ERROR] indexing" $cur/daily_batch.log
    log "[ERROR] source: $source is invalid" $cur/daily_batch.log
fi
