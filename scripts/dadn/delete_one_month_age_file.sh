#!/bin/sh
cur=$(dirname $0)

date=$(date -d '1 day ago' +'%Y-%m-%d')
if [ ! -z $1 ] ; then
    date=$1
fi
echo $date

# delete one month ago file
month_ago=$(date -d '35 days ago' +'%Y-%m-%d')
/opt/hadoop/bin/hdfs dfs -rm -r -f /data/dadn/query/${month_ago}
