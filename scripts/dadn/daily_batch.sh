#!/bin/sh
cur=$(dirname $0)
. $cur/logger.sh

addresses=yuichi.sano@d2c.co.jp,atsushi.murakami@d2c.co.jp,kentaro.suzuki@d2c.co.jp,kazuyoshi.takayama@d2c.co.jp

date=$1
if [ -z $date ] ; then
    date=$(date +'%Y-%m-%d')  
fi
date2=$(date -d "$date" +'%Y%m%d') 
yesterday=$(date -d "$date 1 day ago" +'%Y-%m-%d') 

# get optout user
$cur/data/get_optout_user.sh

# get segment request
$cur/data/get_segments_request.sh

# extract frequent keywords
log "[START] extract frequent keywords" $cur/daily_batch.log
$cur/dadn_keyword_extractor.sh $yesterday
$cur/dadn_keyword_aggregator.sh $yesterday
log "[END] extract frequent keywords" $cur/daily_batch.log


# get f81 users
log "[START] get_f81_users $yesterday" $cur/daily_batch.log
$cur/data/get_f81_users.sh $yesterday
log "[END] get_f81_users $yesterday" $cur/daily_batch.log

# get filtered_keywords
log "[START] get_filtered_frequent_keywords $yesterday" $cur/daily_batch.log
$cur/data/get_filtered_frequent_keywords.sh $yesterday
log "[END] get_filtered_frequent_keywords $yesterday" $cur/daily_batch.log

# send result mail
f81_users=$(ls $cur/data/f81_users_${yesterday}/)
filtered_kws=$(ls $cur/data/filtered_frequent_keywords_${yesterday}/)
if [ -s $cur/data/f81_users_${yesterday}/users.txt ] && [ -s $cur/data/filtered_frequent_keywords_${yesterday}/source.txt ] ; then
    echo "send mail"
    $cur/mail.py "[r2a018] import f81 users and filtered keywords $date" "$date ${f81_users} ${filtered_kws})" $addresses
fi

# indexing
log "[START] daily_indexing " $cur/daily_batch.log
$cur/daily_indexing.sh
log "[END] daily_indexing " $cur/daily_batch.log

# update index
$cur/update_index.sh

# check optout user
log "[START] check optout user " $cur/daily_batch.log
if [ ! -d $cur/data/optout/${date} ] ; then
    mkdir $cur/data/optout/${date}
    touch $cur/data/optout/${date}/docomo_optout_v1_${date2}.tsv
fi
if [ ! -s $cur/data/optout/data/${date}/docomo_optout_v1_${date2}.tsv ] ; then
    $cur/mail.py "[WARN][r2a018] number of optout users is 0 " "$date $cur/$cur/data/optout/${date}/docomo_optout_v1_${date2}.tsv" $addresses
fi
log "[END] check optout user " $cur/daily_batch.log

# make segments
log "[START] make_segments " $cur/daily_batch.log
$cur/make_segments.sh
if [ -s $cur/data/search_segments/$date/docomo_search_segments_v1_$date2.tsv ] ; then
    contents=$(head $cur/data/search_segments/$date/docomo_search_segments_v1_$date2.tsv)
    $cur/mail.py "[r2a018] made segments for $date " "$contents" $addresses
    echo "send mail $contents"
else
    log "[ERROR] make_segments" $cur/daily_batch.log
    $cur/mail.py "[r2a018] failed to make segments for $date " "" $addresses
fi
log "[END] make_segments " $cur/daily_batch.log

# check user_test
log "[START] check user_test " $cur/daily_batch.log
$cur/data/user_check.sh >$cur/test_users
num_users=$(cat $cur/test_users | wc -l)
echo $num_users
if [ $num_users -ge 2 ] ; then
    echo "OK"
    # upload_segments.sh
    log "[START] upload_segments " $cur/daily_batch.log
    $cur/upload_segments.sh
    log "[END] upload_segments" $cur/daily_batch.log
else
    log "[ERROR] upload_segments" $cur/daily_batch.log
    content=$(cat $cur/test_users)
    $cur/mail.py "[ERROR][r2a018] test users was not included in the segments file" "$date $content" $addresses
fi
log "[END] check user_test " $cur/daily_batch.log
