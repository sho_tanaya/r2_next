#!/bin/sh
cur=$(dirname $0)
configFile=batch_config.txt

date=$(date +'%Y-%m-%d')
if [ ! -z $1 ] ; then
    date=$1
fi

date2=$(date -d "$date" +'%Y%m%d')
yesterday=$(date -d "$date 1 day ago" +'%Y-%m-%d')

echo date=$date > $cur/$configFile
echo date2=$date2 >> $cur/$configFile
echo yesterday=$yesterday >> $cur/$configFile


cat $cur/$configFile
