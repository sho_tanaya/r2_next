#!/bin/sh
cur=$(dirname $0)
. $cur/logger.sh

hadoop_home=/opt/hadoop/

file_prefix=dadn_query3


date=$(date -d '1 day ago' +'%Y-%m-%d')
if [ ! -z $1 ] ; then
    date=$1
fi

fileDate=$(date -d $date +'%Y%m%d')
echo $date $fileDate

fileName=${file_prefix}_${fileDate}.tsv
filePath=${cur}/data/${fileName}

log "[START] export dadn_query log" $cur/daily_batch.log


seg_id_f81_table_date=${date}
hdfs dfs -ls /user/hive/warehouse/seg_id_f81_table/ver=${date}

if [ $? != 0 ]; then
  seg_id_f81_table_date=$(hdfs dfs -ls /user/hive/warehouse/seg_id_f81_table | tail -n 1 | awk '{print $8}' | rev | cut -c 1-10 | rev)
  $cur/send_mail.sh "[operate03] not found today seg_id_f81_table ${date}, use ${seg_id_f81_table_date}" "$content"
fi

if [ ! -s ${filePath} ]; then
  hive --hiveconf ver="${date}" --hiveconf path="$cur/${file_prefix}_tmp" --hiveconf ver2="${seg_id_f81_table_date}" -f $cur/hqls/export_${file_prefix}.hql
  cat $cur/${file_prefix}_tmp/* > ${filePath}
  rm -rf $cur/${file_prefix}_tmp
fi


if [ -s ${filePath} ]; then
    log "[END] export dadn_query log" $cur/daily_batch.log
    content=$(tail -n 10 ${filePath} | nkf -j)
    echo $content
    $cur/send_mail.sh "[operate03] succeeded to export and upload the dadn_query_$date.tsv" "$content" 
    
    # Aurum7
    aurum7=
    /opt/hadoop/bin/hdfs dfs -rm  -f ${aurum7}/data/dadn/query/${date}/query2.tsv
    /opt/hadoop/bin/hdfs dfs -mkdir ${aurum7}/data/dadn/query/${date}
    /opt/hadoop/bin/hdfs dfs -copyFromLocal ${filePath} ${aurum7}/data/dadn/query/${date}/query2.tsv

    # Aurum3
    #aurum3=hdfs://namenode302.aurum.d2c.ne.jp
    #/opt/hadoop/bin/hdfs dfs -rm  -f ${aurum3}/user/r2a/data/dadn/query/${date}/query2.tsv
    #/opt/hadoop/bin/hdfs dfs -mkdir ${aurum3}/user/r2a/data/dadn/query/${date}
    #/opt/hadoop/bin/hdfs dfs -copyFromLocal ${filePath} ${aurum3}/user/r2a/data/dadn/query/${date}/query2.tsv

    #
    /opt/hadoop/bin/hdfs dfs -ls -R ${aurum7}/data/dadn/query/
    /opt/hadoop/bin/hdfs dfs -ls -R ${aurum3}/user/r2a/data/dadn/query/

    rm -rf ${filePath}

    exit 0
else
    content=$(head ${filePath})
    $cur/send_mail.sh "[ERROR][operate03] failed to export and upload the dadn_query_$date.tsv" "$content" 
    log "[ERROR] ${filePath} is invalid" $cur/daily_batch.log

    exit 1
fi

