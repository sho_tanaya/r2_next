#!/bin/sh
cur=$(dirname $0)

. $cur/batch_config.txt

echo $date
if [ ! -d $cur/data/search_segments/$date ] ; then
    mkdir $cur/data/search_segments/$date
    mkdir $cur/data/search_segments_uu/$date
fi

$cur/check_segment_file.sh
$cur/check_uu_file.sh

$cur/sync_segment_file.sh
$cur/sync_uu_file.sh
