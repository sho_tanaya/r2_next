#!/bin/sh
cur=$(dirname $0)
log=$cur/daily_batch.log

addresses=masaki.rikitoku@d2c.co.jp,rikima3132@gmail.com

err=$(tail -n 100 $log | grep -i error)
echo $err
if [ ! -z $err ] ; then
    $cur/mail.py "[R2Docomo ADN Server] ]error in daily_batch.log" $cur/daily_batch.log $addresses
fi