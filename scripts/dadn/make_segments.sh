#!/bin/sh
cur=$(dirname $0)
. $cur/logger.sh
echo $cur

export JAVA_HOME=/usr/java/1.8

today=$(date +'%Y%m%d')
date=$(date +'%Y-%m-%d')
yesterday=$(date -d '1 day ago' +'%Y-%m-%d')
if [ ! -z $1 ] ; then                                                                                                
    date=$1                                                                                                          
    today=$(date -d $date +'%Y%m%d')
fi                                                                                            
shift         
echo $date $today

jars=$cur/target/scala-2.11/r2dadn_server_2.11-1.0-SNAPSHOT.jar
for j in $(ls $cur/target/universal/stage/lib/*.jar) ; do
    jars=$jars:$j
done

# check optout data

rm -rf $cur/data/search_segments/${date}
rm -rf $cur/data/search_segments_uu/${date}
rm -rf $cur/data/search_segments_app/${date}

#if [ ! -d $cur/data/search_segments/${date} ] ; then
    mkdir $cur/data/search_segments/${date}
    mkdir $cur/data/search_segments_uu/${date}
    mkdir $cur/data/search_segments_app/${date}
#fi

prog=com.d2c.bicc.r2a.dadn.UserSegmentMaker

$JAVA_HOME/bin/java -Xmx25000M -cp $jars $prog \
-i $cur/index \
-o $cur/data/optout/data/${date}/docomo_optout_v1_${today}.tsv \
-r $cur/data/search_segments_request/${date}/docomo_search_segments_request_v1_${today}.json \
-f $cur/data/f81_users_${yesterday}/users.txt \
-op $cur/data/search_segments/${date}/docomo_search_segments_v1_${today}.tsv \
-aop $cur/data/search_segments_app/${date}/docomo_app_search_segments_v1_${today}.tsv \
-u $cur/data/search_segments_uu/${date}/docomo_search_segments_uu_v1_${today}.tsv \
 


