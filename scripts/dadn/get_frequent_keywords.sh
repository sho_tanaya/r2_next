#!/bin/sh
cur=$(dirname $0)
r2a_home=/opt/r2a
spark_home=/opt/spark

igo_jar=$spark_home/extjars/igo-xi-0.4.jar
r2a=$cur/target/scala-2.10/r2a_2.10-1.0.jar

ver=$(date -d '1 day ago' +'%Y-%m-%d')
if [ ! -z $1 ] ; then
    ver=$1
fi
echo $ver

log "[START] extract frequent keywords" $cur/daily_batch.log
$cur/dadn_keyword_extractor.sh $yesterday
$cur/dadn_keyword_aggregator.sh $yesterday
log "[END] extract frequent keywords" $cur/daily_batch.log
