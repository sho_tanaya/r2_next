#!/bin/sh
cur=$(dirname $0)
. $cur/logger.sh
. $cur/batch_config.txt

# check uu file
$cur/data/segments_uu_checker.py $cur/data/search_segments_uu/$date/docomo_search_segments_uu_v1_$date2.tsv
if [ "$?" = "0" ] ; then
    # create endfile
    touch $cur/data/search_segments_uu/$date/docomo_search_segments_uu_endfile
    exit 0
else
    log "[ERROR] failed to uu check $cur/data/search_segments/$date/docomo_search_segments_uu_v1_$date2.tsv" $cur/daily_batch.log
    exit 1
fi

