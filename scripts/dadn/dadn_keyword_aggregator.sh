#!/bin/sh
cur=$(dirname $0)
r2a_home=/opt/r2a
spark_home=/opt/spark

igo_jar=$spark_home/extjars/igo-xi-0.4.jar
r2a=$cur/target/scala-2.10/r2a_2.10-1.0.jar

jar=$r2a_home/target/scala-2.10/r2a_2.10-1.0.jar
jars=$r2a_home/target/scala-2.10/r2a_2.10-1.0.jar
for j in $(ls $cur/lib/*.jar) ; do
    jars=$jars,$j
done

ver=$(date -d '1 day ago' +'%Y-%m-%d')
if [ ! -z $1 ] ; then
    ver=$1
fi
echo $ver

program=com.d2c.bicc.r2a.adn.FrequentKeywordAggregator

days=30
ne=18

# delete files
/opt/hadoop/bin/hdfs dfs -rm -r -f /data/dadn/frequent_keywords/$ver/mau2
/opt/hadoop/bin/hdfs dfs -rm -r -f /data/dadn/frequent_keywords/$ver/frequent_keywords2.tsv

$spark_home/bin/spark-submit --num-executors $ne --executor-memory 24G --executor-cores 2 --class $program --master yarn --deploy-mode client --jars $jars $jar \
 -v $ver \
 -d $days
