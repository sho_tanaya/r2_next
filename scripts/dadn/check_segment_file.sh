#!/bin/sh

cur=$(dirname $0)
. $cur/logger.sh
. $cur/batch_config.txt

#check segment file
$cur/data/segments_checker.py $cur/data/search_segments/$date/docomo_search_segments_v1_$date2.tsv
if [ "$?" = "0" ] ; then
    # create endfile
    touch $cur/data/search_segments/$date/docomo_search_segments_endfile
else
    log "[ERROR] failed to check $cur/data/search_segments/$date/docomo_search_segments_v1_$date2.tsv" $cur/daily_batch.log
    exit 1
fi


$cur/data/segments_app_checker.py $cur/data/search_segments_app/$date/docomo_app_search_segments_v1_$date2.tsv
if [ "$?" = "0" ] ; then
    # create endfile
    touch $cur/data/search_segments_app/$date/docomo_app_search_segments_endfile
else
    log "[ERROR] failed to check $cur/data/search_segments_app/$date/docomo_app_search_segments_v1_$date2.tsv" $cur/daily_batch.log
    exit 1
fi

exit 0
