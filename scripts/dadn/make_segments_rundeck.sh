#!/bin/sh
cur=$(dirname $0)

. $cur/batch_config.txt
 
# make segments
$cur/make_segments.sh $date
if [ -s $cur/data/search_segments/$date/docomo_search_segments_v1_$date2.tsv ] ; then
    contents=$(head $cur/data/search_segments/$date/docomo_search_segments_v1_$date2.tsv)
    $cur/send_mail.sh "[r2a018] made segments for $date " "$contents"
    exit 0
else
    $cur/send_mail.sh "[r2a018] failed to make segments for $date " ""
    exit 1
fi


