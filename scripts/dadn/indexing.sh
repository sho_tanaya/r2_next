#!/bin/sh
cur=$(dirname $0)

export JAVA_HOME=/usr/java/1.8

jars=target/scala-2.11/r2dadn_server_2.11-1.0-SNAPSHOT.jar
for jar in $(ls $cur/target/universal/stage/lib/*jar) ; do
    jars="$jars:$jar"
done
#echo $jars

$JAVA_HOME/bin/java -cp $jars com.d2c.bicc.r2a.search.KeywordIndexor $*
