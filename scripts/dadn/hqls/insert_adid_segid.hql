use mymagazine;

set ver="${hiveconf:ver}";

insert into table adid_segid_table
partition (
  ver=${hiveconf:ver},
  sub_ver="00-00-00",
  fname="none"
)
select
  distinct
  acid_adid_suid.adid as adid,
  acid_adid_suid.acid as acid,
  acid_adid_suid.suid as suid,
  segid_suid.seg_id as segid
from
  (select
    acid_adid.adid as adid,
    suid_acid.acctid as acid,
    suid_acid.suid as suid
  from
    (select * from mymagazine.acid_adid_table_prod where ver=${hiveconf:ver}) acid_adid
  join
    (select * from default.acct_table where ver=${hiveconf:ver}) suid_acid
  on (acid_adid.acid = suid_acid.acctid)
  ) acid_adid_suid
  join
  (select * from default.seg_id_sync where ver=${hiveconf:ver} and sub_ver like "00-00-%") segid_suid
  on (acid_adid_suid.suid = segid_suid.suid)
;
