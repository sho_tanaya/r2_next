set ver="${hiveconf:ver}";
set ver2="${hiveconf:ver2}";


insert overwrite local directory "${hiveconf:path}"
select
  distinct
  concat_ws('\t',
    tmp.keyword,
    tmp.seg_id,
    tmp.ver,
    tmp.f81_id,
    tmp.adid,
    tmp.idfa
  ) 
from (
select
  query_f81_adid.keyword,
  query_f81_adid.seg_id,
  query_f81_adid.ver,
  query_f81_adid.f81_id,
  query_f81_adid.adid,
  if(suid_idfa.idfa is null, "null", suid_idfa.idfa) as idfa
from
  (select
    query_f81_acid.keyword as keyword,
    query_f81_acid.seg_id as seg_id,
    query_f81_acid.ver as ver,
    query_f81_acid.suid as suid,
    if(query_f81_acid.f81_id is null, "null", query_f81_acid.f81_id) as f81_id,
    if(acid_adid.adid is null, "null", acid_adid.adid) as adid
  from
    (select
      query_f81_suid.keyword as keyword,
      query_f81_suid.seg_id as seg_id,
      query_f81_suid.ver as ver,
      query_f81_suid.suid as suid,
      query_f81_suid.f81_id as f81_id,
      suid_acid.acctid as acid
    from
      (select
        query_f81.keyword as keyword,
        query_f81.seg_id as seg_id,
        query_f81.ver as ver,
        query_f81.f81_id as f81_id,
        segid_suid.suid as suid
      from
        (select
          query.keyword as keyword,
          query.seg_id as seg_id,
          query.ver as ver,
          f81.f81_id as f81_id
        from
          (select
            keyword,
            user_id as seg_id,
            ver
          from query_for_dsp.dsp_query
          where
            ver=${hiveconf:ver}
            and length(trim(user_id)) > 0
            and keyword not rlike "fin000000|biz000000|shl000000|lif000000|ent000000|bty000000|d20000|じぶんローン|オリックス銀行|スマートニュース|ひし餅"
          group by
            keyword,
            user_id,
            ver
          ) query
          left outer join
          (select
            *
          from
            default.seg_id_f81_table
          where
            ver=${hiveconf:ver2}
            and seg_id is not null) f81
          on (query.seg_id = f81.seg_id)
        ) query_f81
        left outer join
        (select * from default.seg_id_sync_latest_n where ver="latest" and seg_id is not null) segid_suid
        on (query_f81.seg_id = segid_suid.seg_id)
      where
        query_f81.f81_id is not null 
        or segid_suid.suid is not null
      ) query_f81_suid
      left outer join
      (select * from default.acct_table where ver=${hiveconf:ver} and suid is not null) suid_acid
      on (query_f81_suid.suid = suid_acid.suid)
    ) query_f81_acid
    left outer join
    (select * from mymagazine.acid_adid_table_prod where ver=${hiveconf:ver} and acid is not null) acid_adid
    on (query_f81_acid.acid = acid_adid.acid)
  ) query_f81_adid
  left outer join 
  (select
    suid_to_app_id.suid as suid,
    app_id_to_idfa.idfa as idfa
  from
  (select app_id, suid from default.app_id_sync_latest_n where ver="latest") suid_to_app_id
  inner join
  (select seg_id as app_id, f81_id as idfa from default.seg_id_f81_app_table where ver=${hiveconf:ver}) app_id_to_idfa
  on (suid_to_app_id.app_id = app_id_to_idfa.app_id)
  ) suid_idfa
  on (query_f81_adid.suid = suid_idfa.suid)
where
  query_f81_adid.adid is not null
  or query_f81_adid.f81_id is not null
  or suid_idfa.idfa is not null
) tmp
;

