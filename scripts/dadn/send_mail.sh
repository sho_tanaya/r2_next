#!/bin/sh
cur=$(dirname $0)

addresses=yuichi.sano@d2c.co.jp,atsushi.murakami@d2c.co.jp,kentaro.suzuki@d2c.co.jp,yuk.suzuki@d2c.co.jp

title=$1
message=$2

echo title:$title
echo message:$message

$cur/mail.py "$title" "$message" "$addresses"
