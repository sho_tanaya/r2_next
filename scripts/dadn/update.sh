#!/bin/sh
cur=$(dirname $0)
. $cur/logger.sh

# check new_index
if [ -d $cur/index_new ] ; then
    rm -rf $cur/index_bk
    cp -r $cur/index $cur/index_bk
    rm -rf $cur/index
    mv $cur/index_new $cur/index
fi
curl http://localhost:9000/r2f/keyword/index/update