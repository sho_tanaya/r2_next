#!/bin/sh
cur=$(dirname $0)
. $cur/logger.sh
export HOME=/home/r2a

# old jyudan servers
# servers=(10.109.44.53 10.109.53.50 10.109.176.136 10.109.176.135)

# new jyudan servers
servers=(10.109.3.247 10.109.78.129 10.109.244.147 10.109.146.208)
for server in ${servers[@]} ; do
    if [ -d $cur/index ] ; then
	scp -i $HOME/.ssh/jyudan_key.pem -r $cur/index ec2-user@$server:/opt/r2dadn_server/index_new
	ssh -t -i $HOME/.ssh/jyudan_key.pem ec2-user@$server "/opt/r2dadn_server/update.sh"
    fi
done
